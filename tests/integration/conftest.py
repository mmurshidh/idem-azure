import asyncio
import copy
import sys
import unittest.mock as mock
from typing import Any
from typing import Callable
from typing import Dict
from typing import List

import dict_tools.data
import pop.hub
import pytest
from pytest_idem import runner

from tests import api_data as data
from tests.integration.cleaner import Cleaner


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["azure"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_azure"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope="module", name="hub")
async def integration_hub(code_dir, event_loop):
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        for dyne in ["idem", "pop_create"]:
            hub.pop.sub.add(dyne_name=dyne)

        hub.pop.config.load(["idem", "pop_create", "acct"], cli="idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}
        yield hub


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test", test=False, tag="fake_|-test_|-tag"
    )

    if not hub.OPT.acct.acct_file:
        raise ConnectionError("No ACCT_FILE in the environment")
    if not hub.OPT.acct.acct_key:
        raise ConnectionError("No ACCT_KEY in the environment")

    await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    acct = await hub.acct.init.gather(acct_subs, acct_profile)
    if not acct:
        raise ConnectionError(
            f"No valid credentials found for running this test in {hub.OPT.acct.acct_file}"
        )

    # Add the profile to the account
    ctx.acct = dict_tools.data.SafeNamespaceDict(**acct)

    yield ctx


@pytest.fixture(scope="function")
def url(hub):
    url = copy.deepcopy(data.URL)
    return url


@pytest.fixture(scope="function")
def desc(hub):
    desc = copy.deepcopy(data.DESC)
    return desc


@pytest.fixture(scope="module")
def idem_cli() -> Callable:
    return runner.idem_cli


@pytest.fixture(scope="module")
async def module_cleaner(hub, ctx, idem_cli):
    cleaner_obj = Cleaner(hub, ctx, idem_cli)

    yield cleaner_obj

    await cleaner_obj.clean_resources()


@pytest.fixture(scope="function")
async def function_cleaner(hub, ctx, idem_cli):
    cleaner_obj = Cleaner(hub, ctx, idem_cli)

    yield cleaner_obj

    await cleaner_obj.clean_resources()
