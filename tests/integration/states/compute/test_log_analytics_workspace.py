import copy
import time

import pytest


@pytest.mark.asyncio
async def test_log_analytics_workspace(hub, ctx, resource_group_fixture):
    """
    This test provisions a log analytics workspace, describes them and deletes them.
    """

    # Create log analytics workspace
    workspace_name = "idem-test-log-analytics-workspace-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    subscription_id = ctx.acct.subscription_id
    location = "eastus"
    vm_parameters = {
        "public_network_access_for_ingestion": "Enabled",
        "public_network_access_for_query": "Enabled",
        "sku": {"last_sku_update": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
        "features": {
            "enable_log_access_using_only_resource_permissions": True,
            "legacy": 0,
            "search_version": 1,
        },
        "workspace_capping": {"daily_quota_gb": -1.0},
        "tags": {
            "tagkey-1": "tagvalue-1",
            "tagkey-2": "tagvalue-2",
            "E2ETag": "E2EValue",
            "Environment": "Next",
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create log analytics workspace with --test

    vm_ret = await hub.states.azure.compute.log_analytics_workspace.present(
        test_ctx,
        name=workspace_name,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        subscription_id=subscription_id,
        location=location,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert not vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would create azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.OperationalInsights/workspaces/{workspace_name}"
        == resource_id
    )

    # Create log analytics workspace in real
    vm_ret = await hub.states.azure.compute.log_analytics_workspace.present(
        ctx,
        name=workspace_name,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        subscription_id=subscription_id,
        location=location,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert not vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Created azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.OperationalInsights/workspaces/{workspace_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-12-01-preview",
        retry_count=5,
        retry_period=25,
    )

    # Describe log analytics workspace
    describe_ret = await hub.states.azure.compute.log_analytics_workspace.describe(
        ctx,
    )

    resource_group_name_capital = resource_group_name.upper()
    describe_resource_id = f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.OperationalInsights/workspaces/{workspace_name}"
    assert describe_resource_id in describe_ret
    describe_resource = describe_ret.get(describe_resource_id)
    described_resource = describe_resource.get("azure.compute.log_analytics.present")
    # described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource,
        expected_old_state=None,
        expected_new_state=described_resource,
        workspace_name=workspace_name,
        resource_group_name=resource_group_name_capital,
        idem_resource_name=describe_resource_id,
    )

    workspace_update_parameters = {
        "public_network_access_for_ingestion": "Disabled",
        "public_network_access_for_query": "Disabled",
        "sku": {"last_sku_update": "2022-12-13T05:51:07.1724412Z", "name": "pergb2018"},
        "features": {
            "enable_log_access_using_only_resource_permissions": False,
            "legacy": 0,
            "search_version": 1,
        },
        "workspace_capping": {"daily_quota_gb": -1.0},
        "tags": {
            "tagkey-1": "tagvalue-1",
            "tagkey-2": "tagvalue-2",
            "E2ETag": "E2EValue",
            "Environment": "Next",
        },
    }

    # Update log analytics workspace with --test
    vm_ret = await hub.states.azure.compute.log_analytics_workspace.present(
        test_ctx,
        name=workspace_name,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        subscription_id=subscription_id,
        location=location,
        **workspace_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would update azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_ret["comment"]
    )
    check_returned_states(
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=workspace_update_parameters,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.OperationalInsights/workspaces/{workspace_name}"
        == resource_id
    )

    # Update log analytics workspace in real
    vm_ret = await hub.states.azure.compute.log_analytics_workspace.present(
        ctx,
        name=workspace_name,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        subscription_id=subscription_id,
        location=location,
        **workspace_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Updated azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_ret["comment"]
    )
    check_returned_states(
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=workspace_update_parameters,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.OperationalInsights/workspaces/{workspace_name}"
        == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-12-01-preview",
        retry_count=5,
        retry_period=25,
    )

    # Delete log analytics workspace with --test
    vm_del_ret = await hub.states.azure.compute.log_analytics_workspace.absent(
        test_ctx,
        name=workspace_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"Would delete azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_del_ret["comment"]
    )
    check_returned_states(
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=workspace_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )

    # Delete log analytics workspace in real
    vm_del_ret = await hub.states.azure.compute.log_analytics_workspace.absent(
        ctx,
        name=workspace_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]

    assert (
        f"Deleted azure.compute.log_analytics_workspace '{workspace_name}'"
        in vm_del_ret["comment"]
    )
    check_returned_states(
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=workspace_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
        idem_resource_name=workspace_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-12-01-preview",
        retry_count=10,
        retry_period=25,
    )
    # Delete log analytics workspace again
    vm_del_ret = await hub.states.azure.compute.log_analytics_workspace.absent(
        ctx,
        name=workspace_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        workspace_name=workspace_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert not vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"azure.compute.log_analytics_workspace '{workspace_name}' already absent"
        in vm_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    workspace_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert workspace_name == old_state.get("workspace_name")

        assert expected_old_state["features"] == old_state.get("features")
        assert expected_old_state["tags"] == old_state.get("tags")
        expected_old_state_copy = copy.deepcopy(expected_old_state)
        old_state_copy = copy.deepcopy(old_state)
        expected_old_state_copy["sku"]["last_sku_update"] = None
        old_state_copy["sku"]["last_sku_update"] = None
        assert expected_old_state_copy["sku"] == old_state_copy.get("sku")

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert workspace_name == new_state.get("workspace_name")
        assert expected_new_state[
            "public_network_access_for_ingestion"
        ] == new_state.get("public_network_access_for_ingestion")
        assert expected_new_state["public_network_access_for_query"] == new_state.get(
            "public_network_access_for_query"
        )

        assert expected_new_state["features"] == new_state.get("features")
        assert expected_new_state["tags"] == new_state.get("tags")
        expected_new_state_copy = copy.deepcopy(expected_new_state)
        new_state_copy = copy.deepcopy(new_state)
        expected_new_state_copy["sku"]["last_sku_update"] = None
        new_state_copy["sku"]["last_sku_update"] = None
        assert expected_new_state_copy["sku"] == new_state_copy.get("sku")
