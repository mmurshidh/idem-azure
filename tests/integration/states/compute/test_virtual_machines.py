import copy
import time
from collections import ChainMap

import pytest

RESOURCE_TYPE_VM = "compute.virtual_machines"


@pytest.mark.asyncio
async def test_virtual_machine(
    hub,
    ctx,
    resource_group_fixture,
    network_interface_fixture,
    network_interface_update_fixture,
    function_cleaner,
):
    """
    This test provisions a virtual machine, describes them and deletes them.
    """

    # Create VM
    vm_name = "idem-test-virtual-machine-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    network_interface_id = network_interface_fixture.get("id")
    network_interface_update_id = network_interface_update_fixture.get("id")
    subscription_id = ctx.acct.subscription_id
    computer_name = "idem-test-compute-" + str(int(time.time()))
    os_disk_name = "idem-test-os-disk-" + str(int(time.time()))
    data_disk_name = "idem-test-data-disk-" + str(int(time.time()))
    vm_parameters = {
        "location": "eastus",
        "virtual_machine_size": "Standard_B1ls",
        "network_interface_ids": [network_interface_id],
        "storage_image_reference": {
            "image_sku": "18.04-LTS",
            "image_publisher": "Canonical",
            "image_version": "latest",
            "image_offer": "UbuntuServer",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadWrite",
            "disk_size_in_GB": 30,
            "disk_create_option": "FromImage",
            "disk_delete_option": "Delete",
        },
        "storage_data_disks": [
            {
                "disk_name": data_disk_name,
                "disk_size_in_GB": 4,
                "disk_logical_unit_number": 0,
                "disk_caching": "ReadWrite",
                "disk_create_option": "Empty",
                "storage_account_type": "Premium_LRS",
                "disk_delete_option": "Delete",
            }
        ],
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create virtual machine with --test
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        test_ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert not vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would create azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    )
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    # Create virtual machine in real
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] is None and vm_ret["new_state"] is not None
    created_state = vm_ret["new_state"]
    assert created_state.get("resource_id") and created_state.get("name")
    function_cleaner.mark_for_deletion(created_state, RESOURCE_TYPE_VM)
    assert f"Created azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-07-01",
        retry_count=5,
        retry_period=25,
    )

    # Describe virtual machine
    describe_ret = await hub.states.azure.compute.virtual_machines.describe(ctx)

    resource_group_name_capital = resource_group_name.upper()
    describe_resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name_capital}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}"
    )
    assert describe_resource_id in describe_ret
    describe_resource = describe_ret.get(describe_resource_id)
    described_resource = describe_resource.get("azure.compute.virtual_machines.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_both_returned_states(
        hub,
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        virtual_machine_name=vm_name,
        resource_group_name=resource_group_name_capital,
        idem_resource_name=describe_resource_id,
    )

    vm_update_parameters = {
        "location": "eastus",
        "network_interface_ids": [network_interface_update_id],
        "virtual_machine_size": "Standard_B1s",
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "storage_image_reference": {
            "image_sku": "18.04-LTS",
            "image_publisher": "Canonical",
            "image_version": "latest",
            "image_offer": "UbuntuServer",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadWrite",
            "disk_size_in_GB": 30,
            "disk_create_option": "FromImage",
            "disk_delete_option": "Detach",
        },
        "storage_data_disks": [
            {
                "disk_name": data_disk_name,
                "disk_size_in_GB": 4,
                "disk_logical_unit_number": 0,
                "disk_caching": "ReadWrite",
                "disk_create_option": "Empty",
                "storage_account_type": "Premium_LRS",
                "disk_delete_option": "Detach",
            }
        ],
        "tags": {"tag-new-key": "tag-new-value"},
    }

    # Update virtual machine with --test
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        test_ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would update azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    )
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=vm_update_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    # Update virtual machine in real
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert f"Updated azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=vm_update_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-07-01",
        retry_count=5,
        retry_period=25,
    )

    # Delete virtual machine with --test
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        test_ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"Would delete azure.compute.virtual_machines '{vm_name}'"
        in vm_del_ret["comment"]
    )
    check_both_returned_states(
        hub,
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=vm_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )

    # Delete virtual machine in real
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]

    assert (
        f"Deleted azure.compute.virtual_machines '{vm_name}'" in vm_del_ret["comment"]
    )
    check_both_returned_states(
        hub,
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=vm_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-07-01",
        retry_count=10,
        retry_period=25,
    )
    function_cleaner.mark_deleted(resource_id)

    # Delete virtual machine again
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert not vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"azure.compute.virtual_machines '{vm_name}' already absent"
        in vm_del_ret["comment"]
    )


"""
If deployment fails with Bad Request and error code ResourcePurchaseValidationFailed,
then the plan's terms are most likely not accepted by the executing subscription.
This has to be done manually using following steps with Azure CLI:
az login
az account set --subscription "<subscription_name>"
az vm image show --location eastus --urn microsoft-ads:windows-data-science-vm:windows2016:latest
- check that it has plan property
az vm image terms show --urn microsoft-ads:windows-data-science-vm:windows2016:latest
- check that "accepted" is false
az vm image terms accept --urn microsoft-ads:windows-data-science-vm:windows2016:latest
- check that now "accepted" is true
"""


@pytest.mark.asyncio
async def test_deploy_vm_with_plan(
    hub, ctx, resource_group_fixture, network_interface_fixture, function_cleaner
):
    """
    This test provisions a virtual machine, which requires a plan to deploy
    """
    # Create VM
    vm_name = "idem-test-virtual-machine-plan-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    network_interface_id = network_interface_fixture.get("id")
    subscription_id = ctx.acct.subscription_id
    computer_name = "plan" + str(int(time.time()))
    os_disk_name = "idem-test-os-disk-plan-" + str(int(time.time()))
    vm_parameters = {
        "location": "eastus",
        "virtual_machine_size": "Standard_B1ls",
        "network_interface_ids": [network_interface_id],
        "plan": {
            "publisher": "microsoft-ads",
            "product": "windows-data-science-vm",
            "name": "windows2016",
        },
        "storage_image_reference": {
            "image_sku": "windows2016",
            "image_publisher": "microsoft-ads",
            "image_version": "latest",
            "image_offer": "windows-data-science-vm",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadOnly",
            "disk_create_option": "FromImage",
            "disk_delete_option": "Delete",
        },
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    }
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] is None and vm_ret["new_state"] is not None
    created_state = vm_ret["new_state"]
    assert created_state.get("resource_id") and created_state.get("name")
    function_cleaner.mark_for_deletion(created_state, RESOURCE_TYPE_VM)
    assert f"Created azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )


@pytest.mark.asyncio
async def test_deploy_licensed_vm(
    hub, ctx, resource_group_fixture, network_interface_fixture, function_cleaner
):
    """
    This test provisions a virtual machine, which requires a plan to deploy
    """
    # Create VM
    vm_name = "idem-test-virtual-machine-license-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    network_interface_id = network_interface_fixture.get("id")
    subscription_id = ctx.acct.subscription_id
    os_disk_name = "idem-test-os-disk-license-" + str(int(time.time()))
    vm_parameters = {
        "location": "eastus",
        "virtual_machine_size": "Standard_F2s_v2",
        "network_interface_ids": [network_interface_id],
        "storage_image_reference": {
            "image_sku": "2022-Datacenter",
            "image_publisher": "MicrosoftWindowsServer",
            "image_version": "latest",
            "image_offer": "WindowsServer",
        },
        "storage_os_disk": {
            "storage_account_type": "Premium_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadWrite",
            "disk_create_option": "FromImage",
            "disk_delete_option": "Delete",
        },
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": "license",
            "admin_password": "admin-password$A",
        },
        "license_type": "Windows_Server",
    }
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] is None and vm_ret["new_state"] is not None
    created_state = vm_ret["new_state"]
    assert created_state.get("resource_id") and created_state.get("name")
    function_cleaner.mark_for_deletion(created_state, RESOURCE_TYPE_VM)
    assert f"Created azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )


@pytest.mark.asyncio
async def test_deploy_vm_with_boot_diagnostics_enabled(
    hub, ctx, resource_group_fixture, network_interface_fixture, function_cleaner
):
    # Create VM
    vm_name = "idem-test-virtual-machine-bootd-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    network_interface_id = network_interface_fixture.get("id")
    subscription_id = ctx.acct.subscription_id
    computer_name = "bootd" + str(int(time.time()))
    os_disk_name = "idem-test-os-disk-bootd-" + str(int(time.time()))
    vm_parameters = {
        "location": "eastus",
        "virtual_machine_size": "Standard_D4ds_v4",
        "network_interface_ids": [network_interface_id],
        "storage_image_reference": {
            "image_sku": "87-gen2",
            "image_publisher": "RedHat",
            "image_version": "8.7.2023022801",
            "image_offer": "RHEL",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadOnly",
            "disk_create_option": "FromImage",
            "disk_delete_option": "Delete",
        },
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "boot_diagnostics": {"enabled": True},
    }
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] is None and vm_ret["new_state"] is not None
    created_state = vm_ret["new_state"]
    assert created_state.get("resource_id") and created_state.get("name")
    function_cleaner.mark_for_deletion(created_state, RESOURCE_TYPE_VM)
    assert f"Created azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    vm_update_parameters = {"boot_diagnostics": {"enabled": False}}

    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        # add required props from old state
        location=created_state.get("location"),
        network_interface_ids=created_state.get("network_interface_ids"),
        os_profile=created_state.get("os_profile"),
        storage_os_disk=created_state.get("storage_os_disk"),
        **vm_update_parameters,
    )

    assert vm_ret["result"], vm_ret["comment"]
    assert f"Updated azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_both_returned_states(
        hub,
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state={**vm_parameters, **vm_update_parameters},
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )

    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )


def check_returned_states(
    hub,
    actual_state,
    expected_state,
    resource_group_name,
    virtual_machine_name,
    idem_resource_name,
):
    hub.tool.azure.test_utils.check_actual_includes_expected(
        actual_state,
        expected_state,
        [
            "os_profile.admin_password",
            "storage_os_disk.disk_id",
            "storage_data_disks[].disk_id",
        ],
    )

    if actual_state:
        assert idem_resource_name == actual_state.get("name")
        assert resource_group_name == actual_state.get("resource_group_name")
        assert virtual_machine_name == actual_state.get("virtual_machine_name")


def check_both_returned_states(
    hub,
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    virtual_machine_name,
    idem_resource_name,
):
    check_returned_states(
        hub,
        old_state,
        expected_old_state,
        resource_group_name,
        virtual_machine_name,
        idem_resource_name,
    )
    check_returned_states(
        hub,
        new_state,
        expected_new_state,
        resource_group_name,
        virtual_machine_name,
        idem_resource_name,
    )


async def delete_vm(
    hub, ctx, subscription_id: str, resource_group_name: str, vm_name: str
):
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )

    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]

    assert (
        f"Deleted azure.compute.virtual_machines '{vm_name}'" in vm_del_ret["comment"]
    )

    resource_id = f"/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Compute/virtualMachines/{vm_name}"
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-07-01",
        retry_count=10,
        retry_period=25,
    )
