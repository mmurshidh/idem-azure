import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_resource_group(hub, ctx):
    """
    This test provisions a resource group, describes resource group and deletes the provisioned resource group.
    """
    # Create resource group
    resource_group_name = "idem-test-resource-group-" + str(int(time.time()))
    rg_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create resource group with --test
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        test_ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        **rg_parameters,
    )
    assert rg_ret["result"], rg_ret["comment"]
    assert not rg_ret["old_state"] and rg_ret["new_state"]
    assert (
        f"Would create azure.resource_management.resource_groups '{resource_group_name}'"
        in rg_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=rg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=rg_parameters,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )
    resource_id = rg_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        == resource_id
    )

    # Create resource group in real
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        **rg_parameters,
    )
    assert rg_ret["result"], rg_ret["comment"]
    assert not rg_ret["old_state"] and rg_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=rg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=rg_parameters,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )
    resource_id = rg_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=5,
        retry_period=10,
    )

    # Describe resource group
    describe_ret = await hub.states.azure.resource_management.resource_groups.describe(
        ctx
    )
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.resource_management.resource_groups.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_id,
    )

    rg_update_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
    }
    # Update resource group with --test
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        test_ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        **rg_update_parameters,
    )
    assert rg_ret["result"], rg_ret["comment"]
    assert rg_ret["old_state"] and rg_ret["new_state"]
    assert (
        f"Would update azure.resource_management.resource_groups '{resource_group_name}'"
        in rg_ret["comment"]
    )
    check_returned_states(
        old_state=rg_ret["old_state"],
        new_state=rg_ret["new_state"],
        expected_old_state=rg_parameters,
        expected_new_state=rg_update_parameters,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )
    resource_id = rg_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        == resource_id
    )

    # Update resource group in real
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        **rg_update_parameters,
    )
    assert rg_ret["result"], rg_ret["comment"]
    assert rg_ret["old_state"] and rg_ret["new_state"]
    assert (
        f"Updated azure.resource_management.resource_groups '{resource_group_name}'"
        in rg_ret["comment"]
    )
    check_returned_states(
        old_state=rg_ret["old_state"],
        new_state=rg_ret["new_state"],
        expected_old_state=rg_parameters,
        expected_new_state=rg_update_parameters,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )
    resource_id = rg_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=5,
        retry_period=10,
    )

    # Delete resource group with --test
    rg_del_ret = await hub.states.azure.resource_management.resource_groups.absent(
        test_ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
    )
    assert rg_del_ret["result"], rg_del_ret["comment"]
    assert rg_del_ret["old_state"] and not rg_del_ret["new_state"]
    assert (
        f"Would delete azure.resource_management.resource_groups '{resource_group_name}'"
        in rg_del_ret["comment"]
    )
    check_returned_states(
        old_state=rg_del_ret["old_state"],
        new_state=None,
        expected_old_state=rg_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )

    # Delete resource group
    rg_del_ret = await hub.states.azure.resource_management.resource_groups.absent(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
    )
    assert rg_del_ret["result"], rg_del_ret["comment"]
    assert rg_del_ret["old_state"] and not rg_del_ret["new_state"]
    assert (
        f"Deleted azure.resource_management.resource_groups '{resource_group_name}'"
        in rg_del_ret["comment"]
    )
    check_returned_states(
        old_state=rg_del_ret["old_state"],
        new_state=None,
        expected_old_state=rg_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_group_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete resource group again
    rg_del_ret = await hub.states.azure.resource_management.resource_groups.absent(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
    )
    assert rg_del_ret["result"], rg_del_ret["comment"]
    assert not rg_del_ret["old_state"] and not rg_del_ret["new_state"]
    assert (
        f"azure.resource_management.resource_groups '{resource_group_name}' already absent"
        in rg_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
