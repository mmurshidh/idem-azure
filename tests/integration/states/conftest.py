import random
import time
import uuid
from typing import Any
from typing import Dict

import pytest


@pytest.fixture(scope="module")
async def management_group_fixture(hub, ctx) -> Dict[str, Any]:
    """
    Fixture to create a new management group.
    This management group gets deleted during test clean-up to ensure that no
    testing resources get left over.
    """
    management_group_name = "idem-fixture-management-group-" + str(uuid.uuid4())
    mg_ret = await hub.states.azure.management_groups.management_groups.present(
        ctx,
        name=management_group_name,
        management_group_name=management_group_name,
    )
    if mg_ret["result"]:
        mg_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/providers/Microsoft.Management/managementGroups/{management_group_name}?api-version=2020-05-01",
            retry_count=60,
            retry_period=10,
            retry_policy=[403],
        )
        hub.log.debug(
            f"Attach subscription successfully created management group : {management_group_name} time- "
            + str(int(time.time()))
        )
        yield mg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request management group {management_group_name}  "
            f"error: {mg_ret['comment']} {mg_ret['ret']}"
        )
    delete_ret = await hub.states.azure.management_groups.management_groups.absent(
        ctx, name=management_group_name, management_group_name=management_group_name
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully deleted management group {management_group_name}")
    else:
        raise RuntimeError(
            f"Failed to delete management group {management_group_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def subscription_fixture(hub, ctx) -> Dict[str, Any]:
    """
    Fixture to create a new subscription. This subscription gets deleted during test clean-up to ensure that no
    testing resources get left over.
    """
    subscription_name = "idem-fixture-subscription-" + str(uuid.uuid4())
    sub_ret = await hub.states.azure.subscription.subscriptions.present(
        ctx,
        name=subscription_name,
        alias=subscription_name,
        billing_scope="/providers/Microsoft.Billing/billingAccounts/55263077/enrollmentAccounts/303385",
        display_name="idem-fixture-subscription",
        workload="Production",
    )
    if sub_ret["result"]:
        sub_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/providers/Microsoft.Subscription/aliases/{subscription_name}?api-version=2020-09-01",
            retry_count=5,
            retry_period=10,
        )
        yield sub_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request subscription {subscription_name}  "
            f"error: {sub_ret['comment']} {sub_ret['ret']}"
        )
    delete_ret = await hub.states.azure.subscription.subscriptions.absent(
        ctx, name=subscription_name, alias=subscription_name
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully deleted subscription {subscription_name}")
    else:
        raise RuntimeError(
            f"Failed to delete subscription {subscription_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def resource_group_fixture(hub, ctx) -> Dict[str, Any]:
    """
    Fixture to create a new resource group. This resource group gets deleted during test clean-up to ensure that no
    testing resources get left over.
    """
    resource_group_name = "idem-fixture-resource-group-" + str(int(time.time()))
    location = "eastus"
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        location=location,
    )
    if rg_ret["result"]:
        rg_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
            retry_count=5,
            retry_period=5,
        )
        yield rg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request resource group {resource_group_name} with location {location} with "
            f"error: {rg_ret['comment']} {rg_ret['ret']}"
        )

    delete_ret = await hub.states.azure.resource_management.resource_groups.absent(
        ctx, name=resource_group_name, resource_group_name=resource_group_name
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete resource group {resource_group_name}")
    else:
        raise RuntimeError(
            f"Failed to delete resource group {resource_group_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def policy_definition_fixture(hub, ctx) -> Dict[str, Any]:
    """
    Fixture to create a new policy definition. This policy definition gets deleted during test clean-up to ensure that no
    testing resources get left over.
    """
    policy_definition_name = "idem-fixture-policy-definition-" + str(int(time.time()))
    pd_parameters = {
        "policy_type": "Custom",
        "mode": "Indexed",
        "display_name": "test-location-policy-definition-" + str(int(time.time())),
        "description": "Idem location policy definition for testing",
        "policy_rule": {
            "if": {
                "not": {"field": "location", "in": "[parameters('allowedLocations')]"}
            },
            "then": {"effect": "deny"},
        },
        "parameters": {
            "allowedLocations": {
                "type": "Array",
                "metadata": {
                    "displayName": "Allowed locations",
                    "description": "The list of locations that can be specified when deploying the resources",
                    "strongType": "location",
                },
            },
        },
    }
    rg_ret = await hub.states.azure.policy.policy_definitions.present(
        ctx,
        name=policy_definition_name,
        policy_definition_name=policy_definition_name,
        **pd_parameters,
    )
    if rg_ret["result"]:
        rg_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{policy_definition_name}?api-version=2021-06-01",
            retry_count=5,
            retry_period=5,
        )
        yield rg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request policy definition {policy_definition_name} with "
            f"error: {rg_ret['comment']} {rg_ret['ret']}"
        )

    delete_ret = await hub.states.azure.policy.policy_definitions.absent(
        ctx,
        name=policy_definition_name,
        policy_definition_name=policy_definition_name,
        subscription_id=ctx.acct.subscription_id,
    )
    if delete_ret["result"]:
        hub.log.debug(
            f"Successfully deleted policy definition {policy_definition_name}"
        )
    else:
        raise RuntimeError(
            f"Failed to delete policy definition {policy_definition_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def virtual_network_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new virtual network. This virtual network gets deleted during test clean-up.
    """
    virtual_network_name = "idem-fixture-virtual-network-" + str(
        random.randint(0, 1000)
    )
    resource_group_name = resource_group_fixture.get("name")
    vnet_parameters = {
        "location": "eastus",
        "address_space": ["10.0.0.0/26"],
        "flow_timeout_in_minutes": 10,
    }
    vnet_ret = await hub.states.azure.network.virtual_networks.present(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        **vnet_parameters,
    )
    if vnet_ret["result"]:
        vnet_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield vnet_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request virtual network {virtual_network_name} with parameters {vnet_parameters} with "
            f"error: {vnet_ret['comment']}"
        )
    delete_ret = await hub.states.azure.network.virtual_networks.absent(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete virtual network {virtual_network_name}")
    else:
        raise RuntimeError(
            f"Failed to delete virtual network {virtual_network_name} with error:"
            f" {delete_ret['comment']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def subnet_fixture(hub, ctx, resource_group_fixture, virtual_network_fixture):
    """
    Fixture to create a new subnet. This subnet gets deleted during test clean-up.
    """
    subnet_name = "idem-test-subnet-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = virtual_network_fixture.get("name")
    subnet_parameters = {
        "address_prefix": "10.0.0.0/28",
    }
    subnet_ret = await hub.states.azure.network.subnets.present(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        **subnet_parameters,
    )
    if subnet_ret["result"]:
        subnet_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
            f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield subnet_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request subnet {subnet_name} with parameters {subnet_parameters} with "
            f"error: {subnet_ret['comment']} {subnet_ret.get('ret', '')}"
        )
    delete_ret = await hub.states.azure.network.subnets.absent(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete subnet {subnet_name}")
    else:
        raise RuntimeError(
            f"Failed to delete subnet {subnet_name} with error:"
            f" {delete_ret['comment']} {delete_ret['new_state']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="function")
async def network_interface_fixture(hub, ctx, resource_group_fixture, subnet_fixture):
    """
    Fixture to create a new network interface. This network interface gets deleted during test clean-up.
    """
    # Create network interface
    resource_group_name = resource_group_fixture.get("name")
    network_interface_name = "idem-test-network_interface-" + str(int(time.time()))
    subnet_id = subnet_fixture.get("id")
    nic_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
        "ip_configurations": [
            {
                "name": "test-ipc",
                "private_ip_address_allocation": "Static",
                "subnet_id": subnet_id,
                "private_ip_address_version": "IPv4",
                "private_ip_address": "10.0.0.12",
                "primary": True,
            }
        ],
    }
    nic_ret = await hub.states.azure.network.network_interfaces.present(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_parameters,
    )
    if nic_ret["result"]:
        nic_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
            f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        subnet_fixture.get("id")
        yield nic_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Failed to request Network Interface {network_interface_name} with parameters {nic_parameters} with "
            f"error: {nic_ret['comment']} {nic_ret.get('ret', '')}"
        )
    nic_del_ret = await hub.states.azure.network.network_interfaces.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    if nic_del_ret["result"]:
        hub.log.debug(f"Successfully delete subnet {network_interface_name}")
    else:
        raise RuntimeError(
            f"Failed to delete Network Interface {network_interface_name} with error:"
            f" {nic_del_ret['comment']} {nic_del_ret['new_state']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def network_interface_update_fixture(
    hub, ctx, resource_group_fixture, subnet_fixture
):
    """
    Fixture to create a new network interface for Virtual Machine update operation. This network interface gets deleted during test clean-up.
    """
    # Create network interface
    resource_group_name = resource_group_fixture.get("name")
    network_interface_name = "idem-test-network_interface-" + str(int(time.time()))
    subnet_id = subnet_fixture.get("id")
    nic_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
        "ip_configurations": [
            {
                "name": "test-ipc-updated",
                "private_ip_address_allocation": "Static",
                "subnet_id": subnet_id,
                "private_ip_address_version": "IPv4",
                "private_ip_address": "10.0.0.8",
                "primary": True,
            }
        ],
    }
    nic_ret = await hub.states.azure.network.network_interfaces.present(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_parameters,
    )
    if nic_ret["result"]:
        nic_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
            f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield nic_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Failed to request Network Interface {network_interface_name} with parameters {nic_parameters} with "
            f"error: {nic_ret['comment']} {nic_ret.get('ret', '')}"
        )
    nic_del_ret = await hub.states.azure.network.network_interfaces.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    if nic_del_ret["result"]:
        hub.log.debug(f"Successfully delete subnet {network_interface_name}")
    else:
        raise RuntimeError(
            f"Failed to delete Network Interface {network_interface_name} with error:"
            f" {nic_del_ret['comment']} {nic_del_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def network_security_group_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new network security group. This network security group gets deleted during test clean-up.
    """
    sg_name = "idem-fixture-network-security-group-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    sg_parameters = {"location": "eastus"}
    sg_ret = await hub.states.azure.network.network_security_groups.present(
        ctx,
        name=sg_name,
        resource_group_name=resource_group_name,
        network_security_group_name=sg_name,
        **sg_parameters,
    )
    if sg_ret["result"]:
        sg_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{sg_name}?api-version=2022-07-01",
            retry_count=10,
            retry_period=10,
        )
        yield sg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request network security group {sg_name} with parameters {sg_parameters} with "
            f"error: {sg_ret['comment']} {sg_ret.get('ret', '')}"
        )
    delete_ret = await hub.states.azure.network.network_security_groups.absent(
        ctx,
        name=sg_name,
        resource_group_name=resource_group_name,
        network_security_group_name=sg_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete network security group {sg_name}")
    else:
        raise RuntimeError(
            f"Failed to delete network security group {sg_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="module")
async def public_ip_address_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new public ip address. This public ip address gets deleted during test clean-up.
    """
    pub_ip_name = "idem-test-public-ip-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    pub_ip_parameters = {"location": "eastus"}
    pub_ip_ret = await hub.states.azure.virtual_networks.public_ip_addresses.present(
        ctx,
        name=pub_ip_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=pub_ip_name,
        parameters=pub_ip_parameters,
    )
    if pub_ip_ret["result"]:
        pub_ip_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/publicIPAddresses/{pub_ip_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield pub_ip_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request public ip address {pub_ip_name} with parameters {pub_ip_parameters} with "
            f"error: {pub_ip_ret['comment']} {pub_ip_ret['ret']}"
        )
    delete_ret = await hub.states.azure.virtual_networks.public_ip_addresses.absent(
        ctx,
        name=pub_ip_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=pub_ip_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete public ip address {pub_ip_name}")
    else:
        raise RuntimeError(
            f"Failed to delete public ip address {pub_ip_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture
async def check_permissions(hub, ctx, resource_group_fixture, request):
    resource_group_name = resource_group_fixture.get("name")
    ret = await hub.exec.azure.permission.list(ctx, resource_group_name)
    assert ret, ret.comment

    param = request.param
    if isinstance(param, str):
        param = [param]

    for permission in param:
        if permission not in ret.ret.value[0]["actions"]:
            raise pytest.skip(f"Action not available: {permission}")


@pytest.fixture(scope="module")
async def route_table_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new virtual network. This virtual network gets deleted during test clean-up.
    """
    route_table_name = "idem-fixture-route-table-" + str(random.randint(0, 1000))
    resource_group_name = resource_group_fixture.get("name")
    rt_parameters = {
        "location": "eastus",
    }
    rt_ret = await hub.states.azure.network.route_tables.present(
        ctx,
        name=route_table_name,
        route_table_name=route_table_name,
        resource_group_name=resource_group_name,
        **rt_parameters,
    )
    if rt_ret["result"]:
        rt_wait_ret = await hub.tool.azure.resource.wait_for_present(
            ctx,
            url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/routeTables/{route_table_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield rt_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request route table {route_table_name} with parameters {rt_parameters} with "
            f"error: {rt_ret['comment']}"
        )
    delete_ret = await hub.states.azure.network.route_tables.absent(
        ctx,
        name=route_table_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete route table {route_table_name}")
    else:
        raise RuntimeError(
            f"Failed to delete route table {route_table_name} with error:"
            f" {delete_ret['comment']}. Manual clean-up may be needed."
        )
