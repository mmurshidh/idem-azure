import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "network.virtual_network_peerings"
RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/virtualNetworkPeerings/{virtual_network_peering_name}"

PROPERTIES = {
    "name": "name",
    "virtual_network_name": "my-vnet",
    "virtual_network_peering_name": "my-vnt-peering",
    "allow_virtual_network_access": True,
    "allow_forwarded_traffic": False,
    "allow_gateway_transit": True,
    "use_remote_gateways": False,
    "remote_virtual_network": {"id": ""},
    "do_not_verify_remote_gateways": False,
}

UPDATED_PROPERTIES = {
    "allow_virtual_network_access": False,
    "allow_forwarded_traffic": True,
    "allow_gateway_transit": False,
}


@pytest.mark.asyncio
@pytest.fixture(scope="module")
async def new_virtual_network(hub, ctx, idem_cli, resource_group_fixture):
    name = hub.tool.azure.test_utils.generate_unique_name("idem-test-virtual-network")
    present_state_vnet = {
        "name": name,
        "resource_group_name": resource_group_fixture.get("name"),
        "virtual_network_name": name,
        "location": "eastus",
        "address_space": ["10.0.0.0/26"],
    }

    vnet_present_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, "network.virtual_networks", present_state_vnet
    )
    assert vnet_present_ret["result"], vnet_present_ret["comment"]

    resource_id = vnet_present_ret["new_state"]["resource_id"]
    assert present_state_vnet.items() <= vnet_present_ret["new_state"].items()

    yield vnet_present_ret["new_state"]

    vnet_absent_ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        "network.virtual_networks",
        vnet_present_ret["new_state"]["name"],
        resource_id,
        absent_params={
            "resource_group_name": vnet_present_ret["new_state"]["resource_group_name"],
            "virtual_network_name": vnet_present_ret["new_state"][
                "virtual_network_name"
            ],
        },
    )
    assert vnet_absent_ret["result"], vnet_absent_ret["comment"]
    assert not vnet_absent_ret.get("new_state")


@pytest.mark.asyncio
@pytest.fixture(scope="module")
async def new_virtual_network_remote(hub, ctx, idem_cli, resource_group_fixture):
    name = hub.tool.azure.test_utils.generate_unique_name("idem-test-virtual-network")
    present_state_vnet = {
        "name": name,
        "resource_group_name": resource_group_fixture.get("name"),
        "virtual_network_name": name,
        "location": "eastus",
        "address_space": ["10.7.0.0/26"],
    }

    vnet_present_ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, "network.virtual_networks", present_state_vnet
    )
    assert vnet_present_ret["result"], vnet_present_ret["comment"]

    resource_id = vnet_present_ret["new_state"]["resource_id"]
    assert present_state_vnet.items() <= vnet_present_ret["new_state"].items()

    yield vnet_present_ret["new_state"]

    vnet_absent_ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        "network.virtual_networks",
        vnet_present_ret["new_state"]["name"],
        resource_id,
        absent_params={
            "resource_group_name": vnet_present_ret["new_state"]["resource_group_name"],
            "virtual_network_name": vnet_present_ret["new_state"][
                "virtual_network_name"
            ],
        },
    )
    assert vnet_absent_ret["result"], vnet_absent_ret["comment"]
    assert not vnet_absent_ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_create")
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_virtual_network_peerings_create(
    hub,
    ctx,
    idem_cli,
    __test,
    resource_group_fixture,
    new_virtual_network,
    new_virtual_network_remote,
):
    name = hub.tool.azure.test_utils.generate_unique_name(
        "idem-test-virtual-network-peering"
    )
    PROPERTIES["name"] = name
    PROPERTIES["virtual_network_peering_name"] = name
    PROPERTIES["subscription_id"] = ctx.acct.get("subscription_id")
    PROPERTIES["resource_group_name"] = resource_group_fixture.get("name")
    PROPERTIES["virtual_network_name"] = new_virtual_network["virtual_network_name"]
    PROPERTIES["remote_virtual_network"]["id"] = new_virtual_network_remote[
        "resource_id"
    ]

    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
            in ret["comment"]
        )
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    expected_state = {"resource_id": resource_id, **PROPERTIES}
    assert ret["new_state"] == expected_state


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_create"])
async def test_virtual_network_peerings_describe(hub, ctx):
    ret = await hub.states.azure.network.virtual_network_peerings.describe(ctx)
    current_resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    for resource_id in ret:
        described_resource = ret[resource_id].get(f"azure.{RESOURCE_TYPE}.present")
        assert described_resource
        resource_state = dict(ChainMap(*described_resource))

        if resource_id == current_resource_id:
            expected_state = {"resource_id": resource_id, **PROPERTIES}
            expected_state["name"] = resource_id
            assert resource_state == expected_state
        else:
            assert resource_state.get("resource_id") == resource_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_virtual_network_peerings_empty_update(hub, ctx, idem_cli, __test):
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    assert ret["old_state"] == ret["new_state"]
    expected_new_state = {
        "resource_id": RESOURCE_ID_FORMAT.format(**PROPERTIES),
        **PROPERTIES,
    }
    assert ret["new_state"] == expected_new_state


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_virtual_network_peerings_update(hub, ctx, idem_cli, __test):
    updated_state = copy.deepcopy(PROPERTIES)
    updated_state.update(UPDATED_PROPERTIES)
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, updated_state, __test
    )

    assert ret["result"], ret["comment"]
    resource_id = RESOURCE_ID_FORMAT.format(**updated_state)
    expected_old_state = {"resource_id": resource_id, **PROPERTIES}
    assert ret["old_state"] == expected_old_state

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
            in ret["comment"]
        )
    expected_new_state = {"resource_id": resource_id, **updated_state}
    assert ret["new_state"] == expected_new_state


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_virtual_network_peerings_absent(hub, ctx, idem_cli, __test):

    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES["name"],
        resource_id,
        test=__test,
        absent_params={
            "resource_group_name": PROPERTIES["resource_group_name"],
            "virtual_network_name": PROPERTIES["virtual_network_name"],
            "virtual_network_peering_name": PROPERTIES["virtual_network_peering_name"],
        },
    )

    assert ret["result"], ret["comment"]
    current_state = copy.deepcopy(PROPERTIES)
    current_state.update(UPDATED_PROPERTIES)
    expected_old_state = {"resource_id": resource_id, **current_state}
    assert ret["old_state"] == expected_old_state
    assert not ret["new_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
            in ret["comment"]
        )
        ret = await hub.exec.azure.network.virtual_network_peerings.get(
            ctx, resource_id=resource_id
        )
        assert ret["result"]
        assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_virtual_network_peerings_already_absent(hub, ctx, idem_cli, __test):

    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx,
        idem_cli,
        RESOURCE_TYPE,
        PROPERTIES["name"],
        resource_id,
        test=__test,
        absent_params={
            "resource_group_name": PROPERTIES["resource_group_name"],
            "virtual_network_name": PROPERTIES["virtual_network_name"],
            "virtual_network_peering_name": PROPERTIES["virtual_network_peering_name"],
        },
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]

    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    ret = await hub.exec.azure.network.virtual_network_peerings.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"]
    assert not ret["ret"]
