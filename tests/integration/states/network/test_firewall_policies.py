import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_firewall_policy_full(hub, ctx, resource_group_fixture):
    """
    This test provisions a firewall policy, describes firewall policy, does a force update and deletes
     the provisioned firewall policy.
    """
    # Create firewall policy
    resource_group_name = resource_group_fixture.get("name")
    firewall_policy_name = "idem-test-firewall-policy-" + str(int(time.time()))
    fp_parameters = {
        "location": "eastus",
        "subscription_id": ctx.acct.subscription_id,
        "tags": {
            f"idem-test-tag-key-"
            + str(uuid.uuid4()): f"idem-test-tag-value-"
            + str(uuid.uuid4())
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create Firewall policy with --test
    fp_ret = await hub.states.azure.network.firewall_policies.present(
        test_ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        **fp_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert not fp_ret["old_state"] and fp_ret["new_state"]
    assert (
        f"Would create azure.network.firewall_policies '{firewall_policy_name}'"
        in fp_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=fp_ret["new_state"],
        expected_old_state=None,
        expected_new_state=fp_parameters,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/firewallPolicies/{firewall_policy_name}"
        == resource_id
    )

    # Create Firewall policies in real
    fp_ret = await hub.states.azure.network.firewall_policies.present(
        ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        **fp_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert not fp_ret["old_state"] and fp_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=fp_ret["new_state"],
        expected_old_state=None,
        expected_new_state=fp_parameters,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/firewallPolicies/{firewall_policy_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe Firewall policies
    describe_ret = await hub.states.azure.network.firewall_policies.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.network.firewall_policies.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=resource_id,
    )

    fp_update_parameters = {
        "location": "eastus",
        "subscription_id": ctx.acct.subscription_id,
        "tags": {
            f"idem-test-tag-key-update-"
            + str(uuid.uuid4()): f"idem-test-tag-update-value-"
            + str(uuid.uuid4())
        },
    }
    # Update firewall policy tag with --test
    fp_ret = await hub.states.azure.network.firewall_policies.present(
        test_ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        **fp_update_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert fp_ret["old_state"] and fp_ret["new_state"]
    assert (
        f"Would update azure.network.firewall_policies '{firewall_policy_name}'"
        in fp_ret["comment"]
    )

    check_returned_states(
        old_state=fp_ret["old_state"],
        new_state=fp_ret["new_state"],
        expected_old_state=fp_parameters,
        expected_new_state=fp_update_parameters,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/firewallPolicies/{firewall_policy_name}"
        == resource_id
    )

    # Update virtual network in real
    fp_ret = await hub.states.azure.network.firewall_policies.present(
        ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        **fp_update_parameters,
    )
    assert fp_ret["result"], fp_ret["comment"]
    assert fp_ret["old_state"] and fp_ret["new_state"]
    assert (
        f"Updated azure.network.firewall_policies '{firewall_policy_name}'"
        in fp_ret["comment"]
    )
    check_returned_states(
        old_state=fp_ret["old_state"],
        new_state=fp_ret["new_state"],
        expected_old_state=fp_parameters,
        expected_new_state=fp_update_parameters,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )
    resource_id = fp_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/firewallPolicies/{firewall_policy_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete virtual network with --test
    vnet_del_ret = await hub.states.azure.network.firewall_policies.absent(
        test_ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"Would delete azure.network.firewall_policies '{firewall_policy_name}'"
        in vnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=fp_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )

    # Delete virtual network in real
    vnet_del_ret = await hub.states.azure.network.firewall_policies.absent(
        ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"Deleted azure.network.firewall_policies '{firewall_policy_name}'"
        in vnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=fp_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        idem_resource_name=firewall_policy_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2022-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete virtual network again
    vnet_del_ret = await hub.states.azure.network.firewall_policies.absent(
        ctx,
        name=firewall_policy_name,
        resource_group_name=resource_group_name,
        firewall_policy_name=firewall_policy_name,
        subscription_id=ctx.acct.subscription_id,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert not vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"azure.network.firewall_policies '{firewall_policy_name}' already absent"
        in vnet_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    firewall_policy_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert firewall_policy_name == old_state.get("firewall_policy_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert firewall_policy_name == new_state.get("firewall_policy_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
