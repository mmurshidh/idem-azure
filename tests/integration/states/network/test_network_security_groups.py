import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_network_security_group(hub, ctx, resource_group_fixture):
    """
    This test provisions a network security group, describes network security group and deletes
     the provisioned network security group.
    """
    # Create network security group with --test
    resource_group_name = resource_group_fixture.get("name")
    network_security_group_name = "idem-test-security-group-" + str(uuid.uuid4())
    parameters = {
        "location": "eastus",
        "security_rules": [
            {
                "name": "rule1",
                "access": "Allow",
                "destination_address_prefix": "*",
                "destination_port_range": "80",
                "direction": "Inbound",
                "priority": 130,
                "protocol": "*",
                "source_address_prefix": "*",
                "source_port_range": "*",
            }
        ],
        "tags": {f"idem-test-name": f"idem-test-value"},
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.azure.network.network_security_groups.present(
        test_ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        **parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would create azure.network.network_security_groups '{network_security_group_name}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}"
        == resource_id
    )

    # Create network security group in real
    ret = await hub.states.azure.network.network_security_groups.present(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        **parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe network security group
    describe_ret = await hub.states.azure.network.network_security_groups.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.network.network_security_groups.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=resource_id,
    )

    # Update network security group in test
    updated_parameters = {
        "location": "eastus",
        "security_rules": [
            {
                "name": "rule1",
                "access": "Allow",
                "destination_address_prefix": "*",
                "destination_port_range": "80",
                "direction": "Inbound",
                "priority": 130,
                "protocol": "*",
                "source_address_prefix": "Internet",
                "source_port_range": "100",
            }
        ],
        "tags": {f"idem-test-name": f"idem-test-new-value"},
    }
    ret = await hub.states.azure.network.network_security_groups.present(
        test_ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        **updated_parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update azure.network.network_security_groups '{network_security_group_name}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=parameters,
        expected_new_state=updated_parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}"
        == resource_id
    )

    # Update network security group in real
    ret = await hub.states.azure.network.network_security_groups.present(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        **updated_parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated azure.network.network_security_groups '{network_security_group_name}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=parameters,
        expected_new_state=updated_parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )
    resource_id = ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Network/networkSecurityGroups/{network_security_group_name}"
        == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete network security group in test
    del_ret = await hub.states.azure.network.network_security_groups.absent(
        test_ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"Would delete azure.network.network_security_groups '{network_security_group_name}'"
        in del_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["old_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )

    # Delete network security group in real
    del_ret = await hub.states.azure.network.network_security_groups.absent(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"Deleted azure.network.network_security_groups '{network_security_group_name}'"
        in del_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["old_state"],
        expected_old_state=None,
        expected_new_state=parameters,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
        idem_resource_name=network_security_group_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete again to check if the resource is absent
    del_ret = await hub.states.azure.network.network_security_groups.absent(
        ctx,
        name=network_security_group_name,
        resource_group_name=resource_group_name,
        network_security_group_name=network_security_group_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert not del_ret.get("old_state") and not del_ret.get("new_state")
    assert (
        f"azure.network.network_security_groups '{network_security_group_name}' already absent"
        in del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    network_security_group_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert network_security_group_name == old_state.get(
            "network_security_group_name"
        )
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["security_rules"] == old_state.get("security_rules")
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert network_security_group_name == new_state.get(
            "network_security_group_name"
        )
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["security_rules"] == new_state.get("security_rules")
        assert expected_new_state["tags"] == new_state.get("tags")
