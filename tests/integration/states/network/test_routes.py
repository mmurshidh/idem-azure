import copy
import random
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_routes(hub, ctx, resource_group_fixture, route_table_fixture):
    """
    This test provisions a route table, describes route table, does a force update and deletes
     the provisioned route table.
    """
    # Create route table
    resource_group_name = resource_group_fixture.get("name")
    route_table_name = route_table_fixture.get("name")
    route_name = (
        "idem-test-routes-" + str(int(time.time())) + str(random.randint(0, 1000))
    )
    rt_parameters = {
        "address_prefix": "10.0.0.0/26",
        "next_hop_type": "VirtualAppliance",
        "next_hop_ip_address": "10.1.0.0",
        "subscription_id": ctx.acct.subscription_id,
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create route table with --test
    rt_ret = await hub.states.azure.network.routes.present(
        test_ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        **rt_parameters,
    )
    assert rt_ret["result"], rt_ret["comment"]
    assert not rt_ret["old_state"] and rt_ret["new_state"]
    assert f"Would create azure.network.routes '{route_name}'" in rt_ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=rt_ret["new_state"],
        expected_old_state=None,
        expected_new_state=rt_parameters,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )
    resource_id = rt_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/routeTables/{route_table_name}/routes/{route_name}"
        == resource_id
    )

    # Create route table in real
    rt_ret = await hub.states.azure.network.routes.present(
        ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        **rt_parameters,
    )
    assert rt_ret["result"], rt_ret["comment"]
    assert not rt_ret["old_state"] and rt_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=rt_ret["new_state"],
        expected_old_state=None,
        expected_new_state=rt_parameters,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )
    resource_id = rt_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/routeTables/{route_table_name}/routes/{route_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe route table
    describe_ret = await hub.states.azure.network.routes.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get("azure.network.routes.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        route_name=route_name,
        route_table_name=route_table_name,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_id,
    )

    rt_update_parameters = {
        "address_prefix": "10.0.0.0/24",
        "next_hop_type": "VirtualAppliance",
        "next_hop_ip_address": "10.1.2.0",
        "subscription_id": ctx.acct.subscription_id,
    }
    # Update route table with --test
    rt_ret = await hub.states.azure.network.routes.present(
        test_ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        **rt_update_parameters,
    )
    assert rt_ret["result"], rt_ret["comment"]
    assert rt_ret["old_state"] and rt_ret["new_state"]
    assert f"Would update azure.network.routes '{route_name}'" in rt_ret["comment"]
    check_returned_states(
        old_state=rt_ret["old_state"],
        new_state=rt_ret["new_state"],
        expected_old_state=rt_parameters,
        expected_new_state=rt_update_parameters,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )
    resource_id = rt_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/routeTables/{route_table_name}/routes/{route_name}"
        == resource_id
    )

    # Update route table in real
    rt_ret = await hub.states.azure.network.routes.present(
        ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        **rt_update_parameters,
    )

    assert rt_ret["result"], rt_ret["comment"]
    assert rt_ret["old_state"] and rt_ret["new_state"]
    assert f"Updated azure.network.routes '{route_name}'" in rt_ret["comment"]
    check_returned_states(
        old_state=rt_ret["old_state"],
        new_state=rt_ret["new_state"],
        expected_old_state=rt_parameters,
        expected_new_state=rt_update_parameters,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )
    resource_id = rt_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/routeTables/{route_table_name}/routes/{route_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete route table with --test
    rt_del_ret = await hub.states.azure.network.routes.absent(
        test_ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
    )
    assert rt_del_ret["result"], rt_del_ret["comment"]
    assert rt_del_ret["old_state"] and not rt_del_ret["new_state"]
    assert f"Would delete azure.network.routes '{route_name}'" in rt_del_ret["comment"]
    check_returned_states(
        old_state=rt_del_ret["old_state"],
        new_state=None,
        expected_old_state=rt_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )

    # Delete route table in real
    rt_del_ret = await hub.states.azure.network.routes.absent(
        ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
    )
    assert rt_del_ret["result"], rt_del_ret["comment"]
    assert rt_del_ret["old_state"] and not rt_del_ret["new_state"]
    assert f"Deleted azure.network.routes '{route_name}'" in rt_del_ret["comment"]
    check_returned_states(
        old_state=rt_del_ret["old_state"],
        new_state=None,
        expected_old_state=rt_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
        idem_resource_name=route_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete route table again
    rt_del_ret = await hub.states.azure.network.routes.absent(
        ctx,
        name=route_name,
        resource_group_name=resource_group_name,
        route_table_name=route_table_name,
        route_name=route_name,
    )
    assert rt_del_ret["result"], rt_del_ret["comment"]
    assert not rt_del_ret["old_state"] and not rt_del_ret["new_state"]
    assert (
        f"azure.network.routes '{route_name}' already absent" in rt_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    route_table_name,
    route_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert route_name == old_state.get("route_name")
        assert route_table_name == old_state.get("route_table_name")
        assert expected_old_state["address_prefix"] == old_state.get("address_prefix")
        assert expected_old_state["next_hop_type"] == old_state.get("next_hop_type")
        assert expected_old_state["next_hop_ip_address"] == old_state.get(
            "next_hop_ip_address"
        )
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert route_name == new_state.get("route_name")
        assert expected_new_state["address_prefix"] == new_state.get("address_prefix")
        assert expected_new_state["next_hop_type"] == new_state.get("next_hop_type")
        assert expected_new_state["next_hop_ip_address"] == new_state.get(
            "next_hop_ip_address"
        )
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
