import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_subnet(hub, ctx, resource_group_fixture, virtual_network_fixture):
    """
    This test provisions a subnet, describes subnets and deletes the provisioned subnet.
    """
    # Create subnet
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = virtual_network_fixture.get("name")
    subnet_name = "idem-test-subnet-" + str(int(time.time()))
    subnet_parameters = {
        "address_prefix": "10.0.0.0/27",
        "enforce_private_link_endpoint_network_policies": True,
        "enforce_private_link_service_network_policies": False,
        "delegations": [
            {"name": "my-delegation", "service": "Microsoft.Sql/managedInstances"}
        ],
        "service_endpoints": ["Microsoft.Sql", "Microsoft.Storage"],
        # TODO: Test applying service_endpoint_policy_ids
        # "service_endpoint_policy_ids": [
        #     "/subscriptions/3d1a2f6d-86de-4f00-bfa5-ad022e400894/resourceGroups/taskforce-azure-plugin-resource-group/providers/Microsoft.Network/serviceEndpointPolicies/taskforce-SEP-1",
        #     "/subscriptions/3d1a2f6d-86de-4f00-bfa5-ad022e400894/resourceGroups/taskforce-azure-plugin-resource-group/providers/Microsoft.Network/serviceEndpointPolicies/taskforce-SEP-2",
        # ],
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create Subnet with --test
    subnet_ret = await hub.states.azure.network.subnets.present(
        test_ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        **subnet_parameters,
    )
    assert subnet_ret["result"], subnet_ret["comment"]
    assert not subnet_ret["old_state"] and subnet_ret["new_state"]
    assert (
        f"Would create azure.network.subnets '{subnet_name}'" in subnet_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=subnet_ret["new_state"],
        expected_old_state=None,
        expected_new_state=subnet_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    resource_id = subnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}"
        == resource_id
    )

    # Create subnet in real
    subnet_ret = await hub.states.azure.network.subnets.present(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        **subnet_parameters,
    )
    assert subnet_ret["result"], subnet_ret["comment"]
    assert not subnet_ret["old_state"] and subnet_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=subnet_ret["new_state"],
        expected_old_state=None,
        expected_new_state=subnet_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    resource_id = subnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe subnet
    describe_ret = await hub.states.azure.network.subnets.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get("azure.network.subnets.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        virtual_network_name=virtual_network_name,
        resource_group_name=resource_group_name,
        subnet_name=subnet_name,
        idem_resource_name=resource_id,
    )

    subnet_update_parameters = {
        "address_prefix": "10.0.0.0/28",
        "enforce_private_link_endpoint_network_policies": False,
        "enforce_private_link_service_network_policies": True,
        "delegations": [
            {"name": "my-delegation-2", "service": "Microsoft.Sql/managedInstances"}
        ],
        "service_endpoints": ["Microsoft.Sql"],
        # TODO: Test applying service_endpoint_policy_ids
        # "service_endpoint_policy_ids": [
        #     "/subscriptions/3d1a2f6d-86de-4f00-bfa5-ad022e400894/resourceGroups/taskforce-azure-plugin-resource-group/providers/Microsoft.Network/serviceEndpointPolicies/taskforce-SEP-1",
        # ],
    }

    # update subnet with --test
    subnet_ret = await hub.states.azure.network.subnets.present(
        test_ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        **subnet_update_parameters,
    )
    assert subnet_ret["result"], subnet_ret["comment"]
    assert subnet_ret["old_state"] and subnet_ret["new_state"]
    assert (
        f"Would update azure.network.subnets '{subnet_name}'" in subnet_ret["comment"]
    )
    check_returned_states(
        old_state=subnet_ret["old_state"],
        new_state=subnet_ret["new_state"],
        expected_old_state=subnet_parameters,
        expected_new_state=subnet_update_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    resource_id = subnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}"
        == resource_id
    )

    # Update virtual network in real
    subnet_ret = await hub.states.azure.network.subnets.present(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        **subnet_update_parameters,
    )
    assert subnet_ret["result"], subnet_ret["comment"]
    assert subnet_ret["old_state"] and subnet_ret["new_state"]
    assert f"Updated azure.network.subnets '{subnet_name}'" in subnet_ret["comment"]
    check_returned_states(
        old_state=subnet_ret["old_state"],
        new_state=subnet_ret["new_state"],
        expected_old_state=subnet_parameters,
        expected_new_state=subnet_update_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    resource_id = subnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete subnet with --test
    subnet_del_ret = await hub.states.azure.network.subnets.absent(
        test_ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    assert subnet_del_ret["result"], subnet_del_ret["comment"]
    assert subnet_del_ret["old_state"] and not subnet_del_ret["new_state"]
    assert (
        f"Would delete azure.network.subnets '{subnet_name}'"
        in subnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=subnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=subnet_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    # Delete subnet in real
    subnet_del_ret = await hub.states.azure.network.subnets.absent(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    assert subnet_del_ret["result"], subnet_del_ret["comment"]
    assert subnet_del_ret["old_state"] and not subnet_del_ret["new_state"]
    assert f"Deleted azure.network.subnets '{subnet_name}'" in subnet_del_ret["comment"]
    check_returned_states(
        old_state=subnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=subnet_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        idem_resource_name=subnet_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete subnet again
    subnet_del_ret = await hub.states.azure.network.subnets.absent(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    assert subnet_del_ret["result"], subnet_del_ret["comment"]
    assert not subnet_del_ret["old_state"] and not subnet_del_ret["new_state"]
    assert (
        f"azure.network.subnets '{subnet_name}' already absent"
        in subnet_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    virtual_network_name,
    subnet_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert virtual_network_name == old_state.get("virtual_network_name")
        assert subnet_name == old_state.get("subnet_name")
        assert expected_old_state["address_prefix"] == old_state.get("address_prefix")
        assert expected_old_state["delegations"] == old_state.get("delegations")
        assert expected_old_state["service_endpoints"] == old_state.get(
            "service_endpoints"
        )
        assert expected_old_state[
            "enforce_private_link_endpoint_network_policies"
        ] == old_state.get("enforce_private_link_endpoint_network_policies")
        assert expected_old_state[
            "enforce_private_link_service_network_policies"
        ] == old_state.get("enforce_private_link_service_network_policies")
        assert expected_old_state.get("service_endpoint_policy_ids") == old_state.get(
            "service_endpoint_policy_ids"
        )

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert virtual_network_name == new_state.get("virtual_network_name")
        assert subnet_name == new_state.get("subnet_name")
        assert expected_new_state["address_prefix"] == new_state.get("address_prefix")
        assert expected_new_state["delegations"] == new_state.get("delegations")
        assert expected_new_state["service_endpoints"] == new_state.get(
            "service_endpoints"
        )
        assert expected_new_state[
            "enforce_private_link_endpoint_network_policies"
        ] == new_state.get("enforce_private_link_endpoint_network_policies")
        assert expected_new_state[
            "enforce_private_link_service_network_policies"
        ] == new_state.get("enforce_private_link_service_network_policies")
        assert expected_new_state.get("service_endpoint_policy_ids") == new_state.get(
            "service_endpoint_policy_ids"
        )
