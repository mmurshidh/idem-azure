import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_storage_accounts(hub, ctx, resource_group_fixture):
    """
    This test provisions a storage account, describes storage account and deletes
     the provisioned storage account.
    """
    # Create storage account
    resource_group_name = resource_group_fixture.get("name")
    account_name = "teststorage" + str(int(time.time()))
    storage_parameters = {
        "location": "eastus",
        "sku_tier": "Standard",
        "sku_name": "Standard_LRS",
        "access_tier": "Hot",
        "account_kind": "StorageV2",
        "is_hns_enabled": True,
        "nfsv3_enabled": True,
        "enable_https_traffic_only": False,
        "min_tls_version": "TLS1_1",
        "allow_blob_public_access": True,
        "allow_shared_key_access": True,
        "network_rules": {
            "default_action": "Deny",
            "bypass": "AzureServices",
            "ip_rule_values": [],
            "virtual_network_subnet_ids": [],
        },
        "routing": {
            "publish_microsoft_endpoints": False,
            "publish_internet_endpoints": True,
            "routing_choice": "InternetRouting",
        },
        "encryption_service": {
            "encryption_key_source": "Microsoft.Storage",
            "queue_encryption_key_type": "Account",
            "file_encryption_key_type": "Account",
            "blob_encryption_key_type": "Account",
        },
        "key_policy": {"key_expiration_period_in_days": 5},
        "tags": {"abc": "abcde"},
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create storage account with --test
    storage_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            **storage_parameters,
        )
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert not storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Would create azure.storage_resource_provider.storage_accounts '{account_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=storage_ret["new_state"],
        expected_old_state=None,
        expected_new_state=storage_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )

    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Storage/storageAccounts/{account_name}" == resource_id
    )

    # Create storage account in real
    storage_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            **storage_parameters,
        )
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert not storage_ret["old_state"] and storage_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=storage_ret["new_state"],
        expected_old_state=None,
        expected_new_state=storage_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Storage/storageAccounts/{account_name}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe storage account
    describe_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.describe(ctx)
    )
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.storage_resource_provider.storage_accounts.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=storage_parameters,
        account_name=account_name,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_id,
    )

    storage_update_parameters = {
        "location": "eastus",
        "sku_tier": "Standard",
        "sku_name": "Standard_LRS",
        "access_tier": "Cool",
        "account_kind": "StorageV2",
        "is_hns_enabled": True,
        "nfsv3_enabled": True,
        "enable_https_traffic_only": False,
        "min_tls_version": "TLS1_2",
        "allow_blob_public_access": True,
        "allow_shared_key_access": True,
        "network_rules": {
            "default_action": "Deny",
            "bypass": "AzureServices",
            "ip_rule_values": [],
            "virtual_network_subnet_ids": [],
        },
        "routing": {
            "publish_microsoft_endpoints": True,
            "publish_internet_endpoints": False,
            "routing_choice": "InternetRouting",
        },
        "encryption_service": {
            "encryption_key_source": "Microsoft.Storage",
            "queue_encryption_key_type": "Account",
            "file_encryption_key_type": "Account",
            "blob_encryption_key_type": "Account",
        },
        "key_policy": {"key_expiration_period_in_days": 5},
        "tags": {"org": "hello", "abc": "12345", "test": "test"},
    }

    # Update storage account with --test
    storage_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.present(
            test_ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            **storage_update_parameters,
        )
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Would update azure.storage_resource_provider.storage_accounts '{account_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=storage_ret["old_state"],
        new_state=storage_ret["new_state"],
        expected_old_state=storage_parameters,
        expected_new_state=storage_update_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Storage/storageAccounts/{account_name}" == resource_id
    )

    # Update storage account in real
    storage_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            **storage_update_parameters,
        )
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Updated azure.storage_resource_provider.storage_accounts '{account_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=storage_ret["old_state"],
        new_state=storage_ret["new_state"],
        expected_old_state=storage_parameters,
        expected_new_state=storage_update_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Storage/storageAccounts/{account_name}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete storage account in test
    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.absent(
            test_ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"Would delete azure.storage_resource_provider.storage_accounts '{account_name}'"
        in storage_del_ret["comment"]
    )
    check_returned_states(
        old_state=storage_del_ret["old_state"],
        new_state=None,
        expected_old_state=storage_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )

    # Delete storage account in real
    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.absent(
            ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"Deleted azure.storage_resource_provider.storage_accounts '{account_name}'"
        in storage_del_ret["comment"]
    )
    check_returned_states(
        old_state=storage_del_ret["old_state"],
        new_state=None,
        expected_old_state=storage_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete storage account again
    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_accounts.absent(
            ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert not storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"azure.storage_resource_provider.storage_accounts '{account_name}' already absent"
        in storage_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    account_name,
    idem_resource_name,
):

    if old_state:
        old_state_to_compare = copy.deepcopy(old_state)
        old_state_to_compare.pop("subscription_id", None)
        old_state_to_compare.pop("resource_id", None)

        expected_old_state_to_compare = copy.deepcopy(expected_old_state)
        expected_old_state_to_compare["resource_group_name"] = resource_group_name
        expected_old_state_to_compare["account_name"] = account_name
        expected_old_state_to_compare["name"] = idem_resource_name

        assert old_state_to_compare == expected_old_state_to_compare
    if new_state:
        new_state_to_compare = copy.deepcopy(new_state)
        new_state_to_compare.pop("subscription_id", None)
        new_state_to_compare.pop("resource_id", None)

        expected_new_state_to_compare = copy.deepcopy(expected_new_state)
        expected_new_state_to_compare["resource_group_name"] = resource_group_name
        expected_new_state_to_compare["account_name"] = account_name
        expected_new_state_to_compare["name"] = idem_resource_name

        assert new_state_to_compare == expected_new_state_to_compare
