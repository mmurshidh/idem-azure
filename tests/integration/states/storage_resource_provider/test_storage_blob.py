import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_storage_blobs(hub, ctx, resource_group_fixture):
    """
    This test provisions a storage blob, describes storage blob and deletes
     the provisioned storage blob.
    """
    # Create storage account
    resource_group_name = resource_group_fixture.get("name")
    account_name = "accname" + str(int(time.time()))
    container_name = "testblob10"
    storage_parameters = {"public_access": "Blob", "metadata": {"tagKey2": "tagValue2"}}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create storage account with --test
    storage_ret = await hub.states.azure.storage_resource_provider.storage_blob.present(
        test_ctx,
        name=container_name,
        resource_group_name=resource_group_name,
        account_name=account_name,
        container_name=container_name,
        **storage_parameters,
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert not storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Would create azure.storage_resource_provider.blob '{container_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=storage_ret["new_state"],
        expected_old_state=None,
        expected_new_state=storage_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )

    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Storage/storageAccounts/{account_name}/blobServices/default/containers/{container_name}"
        == resource_id
    )

    ######################
    # Create storage blob in real
    # 1. First create storage account

    storage_parameters_for_account_creation = {
        "location": "eastus",
        "sku_tier": "Standard",
        "sku_name": "Standard_LRS",
        "account_kind": "StorageV2",
        "is_hns_enabled": True,
        "nfsv3_enabled": True,
        "enable_https_traffic_only": False,
        "min_tls_version": "TLS1_1",
        "allow_blob_public_access": True,
        "allow_shared_key_access": True,
        "network_rules": {
            "default_action": "Deny",
        },
    }
    storage_ret_accounts = (
        await hub.states.azure.storage_resource_provider.storage_accounts.present(
            ctx,
            name=account_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            **storage_parameters_for_account_creation,
        )
    )
    assert storage_ret_accounts["result"], storage_ret_accounts["comment"]
    assert not storage_ret_accounts["old_state"] and storage_ret_accounts["new_state"]
    check_returned_states(
        old_state=None,
        new_state=storage_ret_accounts["new_state"],
        expected_old_state=None,
        expected_new_state=storage_parameters_for_account_creation,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=account_name,
    )
    resource_id = storage_ret_accounts["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Storage/storageAccounts/{account_name}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )

    # 2. Then create the storage blob
    storage_ret = await hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx,
        name=container_name,
        resource_group_name=resource_group_name,
        account_name=account_name,
        container_name=container_name,
        **storage_parameters,
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert not storage_ret["old_state"] and storage_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=storage_ret["new_state"],
        expected_old_state=None,
        expected_new_state=storage_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Storage/storageAccounts/{account_name}/blobServices/default/containers/{container_name}"
        == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )
    #############

    # Describe storage blob
    describe_ret = (
        await hub.states.azure.storage_resource_provider.storage_blob.describe(ctx)
    )
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.storage_resource_provider.storage_blob.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        account_name=account_name,
        resource_group_name=resource_group_name,
        idem_resource_name=container_name,
    )

    #### update in test mode
    storage_blob_update_parameters = {"metadata": {"tagKey1": "tagValue1"}}

    # Update storage account with --test
    storage_ret = await hub.states.azure.storage_resource_provider.storage_blob.present(
        test_ctx,
        name=container_name,
        resource_group_name=resource_group_name,
        account_name=account_name,
        container_name=container_name,
        **storage_blob_update_parameters,
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Would update azure.storage_resource_provider.storage_blob '{container_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=storage_ret["old_state"],
        new_state=storage_ret["new_state"],
        expected_old_state=storage_parameters,
        expected_new_state=storage_blob_update_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Storage/storageAccounts/{account_name}/blobServices/default/containers/{container_name}"
        == resource_id
    )

    storage_ret = await hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx,
        name=container_name,
        resource_group_name=resource_group_name,
        account_name=account_name,
        container_name=container_name,
        **storage_blob_update_parameters,
    )
    assert storage_ret["result"], storage_ret["comment"]
    assert storage_ret["old_state"] and storage_ret["new_state"]
    assert (
        f"Updated azure.storage_resource_provider.storage_blob '{container_name}'"
        in storage_ret["comment"]
    )
    check_returned_states(
        old_state=storage_ret["old_state"],
        new_state=storage_ret["new_state"],
        expected_old_state=storage_parameters,
        expected_new_state=storage_blob_update_parameters,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )
    resource_id = storage_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/"
        f"providers/Microsoft.Storage/storageAccounts/{account_name}/blobServices/default/containers/{container_name}"
        == resource_id
    )

    # Delete storage account in test
    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_blob.absent(
            test_ctx,
            name=container_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            container_name=container_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"Would delete azure.storage_resource_provider.storage_blob '{container_name}'"
        in storage_del_ret["comment"]
    )
    check_returned_states(
        old_state=storage_del_ret["old_state"],
        new_state=None,
        expected_old_state=storage_blob_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )

    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_blob.absent(
            ctx,
            name=container_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            container_name=container_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"Deleted azure.storage_resource_provider.storage_blob '{container_name}'"
        in storage_del_ret["comment"]
    )
    check_returned_states(
        old_state=storage_del_ret["old_state"],
        new_state=None,
        expected_old_state=storage_blob_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )

    storage_del_ret = (
        await hub.states.azure.storage_resource_provider.storage_blob.absent(
            ctx,
            name=container_name,
            resource_group_name=resource_group_name,
            account_name=account_name,
            container_name=container_name,
        )
    )
    assert storage_del_ret["result"], storage_del_ret["comment"]
    assert not storage_del_ret["old_state"] and not storage_del_ret["new_state"]
    assert (
        f"azure.storage_resource_provider.storage_blob '{container_name}' already absent"
        in storage_del_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=None,
        expected_old_state=None,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        account_name=account_name,
        idem_resource_name=container_name,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    account_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert account_name == old_state.get("account_name")
        if old_state.get("location") is not None:
            assert expected_old_state["location"] == old_state.get("location")
        if old_state.get("sku_tier") is not None:
            assert expected_old_state["sku_tier"] == old_state.get("sku_tier")
        if old_state.get("sku_name") is not None:
            assert expected_old_state["sku_name"] == old_state.get("sku_name")
        if old_state.get("account_kind") is not None:
            assert expected_old_state["account_kind"] == old_state.get("account_kind")
        if old_state.get("is_hns_enabled") is not None:
            assert expected_old_state["is_hns_enabled"] == old_state.get(
                "is_hns_enabled"
            )
        if old_state.get("nfsv3_enabled") is not None:
            assert expected_old_state["nfsv3_enabled"] == old_state.get("nfsv3_enabled")
        if old_state.get("enable_https_traffic_only") is not None:
            assert expected_old_state["enable_https_traffic_only"] == old_state.get(
                "enable_https_traffic_only"
            )
        if old_state.get("min_tls_version") is not None:
            assert expected_old_state["min_tls_version"] == old_state.get(
                "min_tls_version"
            )
        if old_state.get("allow_blob_public_access") is not None:
            assert expected_old_state["allow_blob_public_access"] == old_state.get(
                "allow_blob_public_access"
            )
        if old_state.get("allow_shared_key_access") is not None:
            assert expected_old_state["allow_shared_key_access"] == old_state.get(
                "allow_shared_key_access"
            )
        if old_state.get("network_rules") and old_state.get("network_rules").get(
            "default_action"
        ):
            assert expected_old_state["network_rules"][
                "default_action"
            ] == old_state.get("network_rules").get("default_action")

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert account_name == new_state.get("account_name")
        if new_state.get("location") is not None:
            assert expected_new_state["location"] == new_state.get("location")
        if new_state.get("sku_tier") is not None:
            assert expected_new_state["sku_tier"] == new_state.get("sku_tier")
        if new_state.get("sku_name") is not None:
            assert expected_new_state["sku_name"] == new_state.get("sku_name")
        if new_state.get("account_kind") is not None:
            assert expected_new_state["account_kind"] == new_state.get("account_kind")
        if new_state.get("is_hns_enabled") is not None:
            assert expected_new_state["is_hns_enabled"] == new_state.get(
                "is_hns_enabled"
            )
        if new_state.get("nfsv3_enabled") is not None:
            assert expected_new_state["nfsv3_enabled"] == new_state.get("nfsv3_enabled")
        if new_state.get("enable_https_traffic_only") is not None:
            assert expected_new_state["enable_https_traffic_only"] == new_state.get(
                "enable_https_traffic_only"
            )
        if new_state.get("min_tls_version") is not None:
            assert expected_new_state["min_tls_version"] == new_state.get(
                "min_tls_version"
            )
        if new_state.get("allow_blob_public_access") is not None:
            assert expected_new_state["allow_blob_public_access"] == new_state.get(
                "allow_blob_public_access"
            )
        if new_state.get("allow_shared_key_access") is not None:
            assert expected_new_state["allow_shared_key_access"] == new_state.get(
                "allow_shared_key_access"
            )
        if new_state.get("network_rules") and new_state.get("network_rules").get(
            "default_action"
        ):
            assert expected_new_state["network_rules"][
                "default_action"
            ] == new_state.get("network_rules").get("default_action")
