import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_role_assignment_full(hub, ctx):
    """
    This test provisions a role assignment, describes role assignment and deletes
     the provisioned role assignment.
    """
    # Create role assignment
    scope = f"/subscriptions/{ctx.acct.subscription_id}"
    role_definition_id = "acdd72a7-3385-48ef-bd42-f606fba81ae7"
    role_assignment_name = str(uuid.uuid4())
    ra_parameters = {
        "role_definition_id": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{role_definition_id}",
        "principal_id": "1130c2c2-4e96-4ecc-80ed-5797651e18d8",
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create role assignment with --test
    ra_ret = await hub.states.azure.authorization.role_assignments.present(
        test_ctx,
        name=role_assignment_name,
        scope=scope,
        role_assignment_name=role_assignment_name,
        **ra_parameters,
    )
    assert ra_ret["result"], ra_ret["comment"]
    assert not ra_ret["old_state"] and ra_ret["new_state"]
    assert (
        f"Would create azure.authorization.role_assignments '{role_assignment_name}'"
        in ra_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ra_ret["new_state"],
        expected_old_state=None,
        expected_new_state=ra_parameters,
        role_assignment_name=role_assignment_name,
        idem_resource_name=role_assignment_name,
    )
    resource_id = ra_ret["new_state"].get("resource_id")
    assert (
        f"{scope}/providers/Microsoft.Authorization/roleAssignments/{role_assignment_name}"
        == resource_id
    )
    # Create role assignment in real
    ra_ret = await hub.states.azure.authorization.role_assignments.present(
        ctx,
        name=role_assignment_name,
        scope=scope,
        role_assignment_name=role_assignment_name,
        **ra_parameters,
    )
    assert ra_ret["result"], ra_ret["comment"]
    assert not ra_ret["old_state"] and ra_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=ra_ret["new_state"],
        expected_old_state=None,
        expected_new_state=ra_parameters,
        role_assignment_name=role_assignment_name,
        idem_resource_name=role_assignment_name,
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}/{scope}/providers/Microsoft.Authorization/roleAssignments/{role_assignment_name}?api-version=2015-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe role assignment
    describe_ret = await hub.states.azure.authorization.role_assignments.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.authorization.role_assignments.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        role_assignment_name=role_assignment_name,
        idem_resource_name=resource_id,
    )
    # Delete role assignment with --test
    ra_del_ret = await hub.states.azure.authorization.role_assignments.absent(
        test_ctx,
        name=role_assignment_name,
        scope=scope,
        role_assignment_name=role_assignment_name,
    )
    assert ra_del_ret["result"], ra_del_ret["comment"]
    assert ra_del_ret["old_state"] and not ra_del_ret["new_state"]
    assert (
        f"Would delete azure.authorization.role_assignments '{role_assignment_name}'"
        in ra_del_ret["comment"]
    )
    check_returned_states(
        old_state=ra_del_ret["old_state"],
        new_state=None,
        expected_old_state=ra_parameters,
        expected_new_state=None,
        role_assignment_name=role_assignment_name,
        idem_resource_name=role_assignment_name,
    )

    # Delete role assignment
    ra_del_ret = await hub.states.azure.authorization.role_assignments.absent(
        ctx,
        name=role_assignment_name,
        scope=scope,
        role_assignment_name=role_assignment_name,
    )
    assert ra_del_ret["result"], ra_del_ret["comment"]
    assert ra_del_ret["old_state"] and not ra_del_ret["new_state"]
    assert (
        f"Deleted azure.authorization.role_assignments '{role_assignment_name}'"
        in ra_del_ret["comment"]
    )
    check_returned_states(
        old_state=ra_del_ret["old_state"],
        new_state=None,
        expected_old_state=ra_parameters,
        expected_new_state=None,
        role_assignment_name=role_assignment_name,
        idem_resource_name=role_assignment_name,
    )
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}/{scope}/providers/Microsoft.Authorization/roleAssignments/{role_assignment_name}?api-version=2015-07-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete role assignment again
    ra_del_ret = await hub.states.azure.authorization.role_assignments.absent(
        ctx,
        name=role_assignment_name,
        scope=scope,
        role_assignment_name=role_assignment_name,
    )
    assert ra_del_ret["result"], ra_del_ret["comment"]
    assert not ra_del_ret["old_state"] and not ra_del_ret["new_state"]
    assert (
        f"azure.authorization.role_assignments '{role_assignment_name}' already absent"
        in ra_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    role_assignment_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert role_assignment_name == old_state.get("role_assignment_name")
        assert expected_old_state["principal_id"] == old_state.get("principal_id")
        assert expected_old_state["role_definition_id"] == old_state.get(
            "role_definition_id"
        )
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert role_assignment_name == new_state.get("role_assignment_name")
        assert expected_new_state["principal_id"] == new_state.get("principal_id")
        assert expected_new_state["role_definition_id"] == new_state.get(
            "role_definition_id"
        )
