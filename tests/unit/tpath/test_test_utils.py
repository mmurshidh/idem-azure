import pytest

test_dict = {
    "prop1": 0,
    "prop2": "somestr",
    "prop3": [],
    "prop4": [1, 2, 3],
    "prop5": {
        "nested_prop1": 1,
        "nested_prop2": ["element1", "element2"],
        "nested_prop3": {},
    },
    "prop6": None,
    "prop7": "",
}


def test_check_actual_includes_expected_equal_dicts(hub):
    hub.tool.azure.test_utils.check_actual_includes_expected(test_dict, test_dict, [])


def test_check_actual_includes_expected_additional_actual_props(hub):
    test_dict_extended = {
        **test_dict,
        "prop8": "new_prop",
        "prop5": {**test_dict.get("prop5"), "nested_prop4": "new_prop_nested"},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_extended, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_nested_dict_item(hub):
    test_dict_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop1": 2},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_prop_changed, test_dict, []
    )


def test_check_actual_includes_expected_ignore_props_nested_dict_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop1": 2},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, ["prop5.nested_prop1"]
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_added_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {
            **test_dict.get("prop5"),
            "nested_prop2": ["element1", "element2", "element3"],
        },
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_changed_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop2": ["element1", "element100"]},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_changed_prop_removed_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {**test_dict.get("prop5"), "nested_prop2": ["element1"]},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, []
    )


def test_check_actual_includes_expected_ignore_props_nested_list_item(hub):
    test_dict_ignored_prop_changed = {
        **test_dict,
        "prop5": {
            **test_dict.get("prop5"),
            "nested_prop2": ["element1", "element2", "element3"],
        },
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict_ignored_prop_changed, test_dict, ["prop5.nested_prop2"]
    )


@pytest.mark.xfail(reason="Asserts are expected to fail with this input", strict=True)
def test_check_actual_includes_expected_additional_expected_props_fails(hub):
    test_dict_extended = {
        **test_dict,
        "prop8": "new_prop",
        "prop5": {**test_dict.get("prop5"), "nested_prop4": "new_prop_nested"},
    }
    hub.tool.azure.test_utils.check_actual_includes_expected(
        test_dict, test_dict_extended, []
    )
