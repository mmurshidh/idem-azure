def test_cleanup_none_values(hub, mock_hub):
    mock_hub.tool.azure.utils.cleanup_none_values = (
        hub.tool.azure.utils.cleanup_none_values
    )
    input_dict = {
        "a": "value",
        "b": 100,
        "c": None,
        "d": 0,
        "e": "",
        "f": [],
        "g": {},
        "h": None,
    }
    output_dict = mock_hub.tool.azure.utils.cleanup_none_values(input_dict)
    assert output_dict == {
        "a": "value",
        "b": 100,
        "d": 0,
        "e": "",
        "f": [],
        "g": {},
    }


def test_dict_add_nested_key_value_pair_empty(hub, mock_hub):
    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )
    input_dict = {}
    key_path = []
    value = 100
    result = mock_hub.tool.azure.utils.dict_add_nested_key_value_pair(
        input_dict, key_path, value
    )
    assert result == {}
    assert input_dict == {}


def test_dict_add_nested_key_value_pair_top_level(hub, mock_hub):
    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )
    input_dict = {}
    key_path = ["key"]
    value = 100
    result = mock_hub.tool.azure.utils.dict_add_nested_key_value_pair(
        input_dict, key_path, value
    )
    expected_result = {"key": 100}
    assert result == expected_result
    assert input_dict == expected_result


def test_dict_add_nested_key_value_pair_nested_key(hub, mock_hub):
    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )
    input_dict = {}
    key_path = ["parent_key", "child_key"]
    value = 100
    result = mock_hub.tool.azure.utils.dict_add_nested_key_value_pair(
        input_dict, key_path, value
    )
    expected_result = {"parent_key": {"child_key": 100}}
    assert result == expected_result
    assert input_dict == expected_result


def test_dict_add_nested_key_value_pair_existing_parent_key(hub, mock_hub):
    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )
    input_dict = {"parent_key": {"other_child": 200}, "other_key": "other_value"}
    key_path = ["parent_key", "child_key"]
    value = 100
    result = mock_hub.tool.azure.utils.dict_add_nested_key_value_pair(
        input_dict, key_path, value
    )
    expected_result = {
        "parent_key": {"child_key": 100, "other_child": 200},
        "other_key": "other_value",
    }
    assert result == expected_result
    assert input_dict == expected_result


def test_dict_add_nested_key_value_pair_existing_key_overrides(hub, mock_hub):
    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )
    input_dict = {"parent_key": {"child_key": 200}}
    key_path = ["parent_key", "child_key"]
    value = 100
    result = mock_hub.tool.azure.utils.dict_add_nested_key_value_pair(
        input_dict, key_path, value
    )
    expected_result = {
        "parent_key": {
            "child_key": 100,
        }
    }
    assert result == expected_result
    assert input_dict == expected_result
