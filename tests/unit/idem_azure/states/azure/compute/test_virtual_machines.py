import copy
from collections import ChainMap

import pytest

from tests.unit.idem_azure.states.azure.mock_utils import unmock_generic_conversion
from tests.unit.idem_azure.states.azure.mock_utils import (
    unmock_update_payload_generation,
)

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
VM_NAME = "my-vm"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "virtual_machine_size": "Standard_B1ls",
    "network_interface_ids": [
        "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-1"
    ],
    "storage_image_reference": {
        "image_sku": "18.04-LTS",
        "image_publisher": "Canonical",
        "image_version": "latest",
        "image_offer": "UbuntuServer",
    },
    "storage_os_disk": {
        "storage_account_type": "Standard_LRS",
        "disk_name": "myVMosdisk",
        "disk_caching": "ReadWrite",
        "disk_size_in_GB": 30,
        "disk_create_option": "FromImage",
        "disk_delete_option": "Detach",
    },
    "storage_data_disks": [
        {
            "disk_name": "data-disk-1",
            "disk_size_in_GB": 2,
            "disk_logical_unit_number": 1,
            "disk_caching": "None",
            "disk_create_option": "Attach",
            "disk_delete_option": "Detach",
            "storage_account_type": "Premium_LRS",
            "disk_id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-1",
        },
        {
            "disk_name": "data-disk-2",
            "disk_size_in_GB": 4,
            "disk_logical_unit_number": 2,
            "disk_caching": "ReadWrite",
            "disk_create_option": "Empty",
            "storage_account_type": "Premium_LRS",
            "disk_delete_option": "Detach",
            "disk_id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-2",
        },
    ],
    "os_profile": {"admin_username": "admin123", "computer_name": "myVM"},
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
}

RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    "properties": {
        "hardwareProfile": {"vmSize": "Standard_B1ls"},
        "storageProfile": {
            "imageReference": {
                "sku": "18.04-LTS",
                "publisher": "Canonical",
                "version": "latest",
                "offer": "UbuntuServer",
            },
            "osDisk": {
                "caching": "ReadWrite",
                "managedDisk": {"id": None, "storageAccountType": "Standard_LRS"},
                "name": "myVMosdisk",
                "createOption": "FromImage",
                "deleteOption": "Detach",
                "diskSizeGB": 30,
            },
            "dataDisks": [
                {
                    "lun": 1,
                    "name": "data-disk-1",
                    "createOption": "Attach",
                    "caching": "None",
                    "managedDisk": {
                        "storageAccountType": "Premium_LRS",
                        "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-1",
                    },
                    "diskSizeGB": 2,
                    "deleteOption": "Detach",
                },
                {
                    "lun": 2,
                    "name": "data-disk-2",
                    "createOption": "Empty",
                    "caching": "ReadWrite",
                    "managedDisk": {
                        "storageAccountType": "Premium_LRS",
                        "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Compute/disks/data-disk-2",
                    },
                    "diskSizeGB": 4,
                    "deleteOption": "Detach",
                },
            ],
        },
        "osProfile": {
            "adminUsername": "admin123",
            "computerName": "myVM",
            "adminPassword": "adminPassword",
        },
        "networkProfile": {
            "networkInterfaces": [
                {
                    "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-1",
                    "properties": {"primary": True},
                }
            ]
        },
    },
}

RESOURCE_PARAMETERS_UPDATE = {
    "location": "eastus",
    "network_interface_ids": [
        "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-2"
    ],
    "virtual_machine_size": "Standard_B1s",
    "os_profile": {
        "admin_username": "admin123",
        "computer_name": "myVM",
    },
    "storage_image_reference": {
        "image_sku": "18.04-LTS",
        "image_publisher": "Canonical",
        "image_version": "latest",
        "image_offer": "UbuntuServer",
    },
    "storage_os_disk": {
        "storage_account_type": "Standard_LRS",
        "disk_name": "myVMosdisk",
        "disk_caching": "ReadWrite",
        "disk_size_in_GB": 30,
        "disk_create_option": "FromImage",
        "disk_delete_option": "Detach",
    },
    "storage_data_disks": [
        {
            "disk_name": "data-disk-3",
            "disk_size_in_GB": 8,
            "disk_logical_unit_number": 4,
            "disk_caching": "ReadWrite",
            "disk_create_option": "Empty",
            "storage_account_type": "Premium_LRS",
            "disk_delete_option": "Detach",
            "disk_id": "test-id",
        }
    ],
    "tags": {"tag-new-key": "tag-new-value"},
}

RESOURCE_PARAMETERS_UPDATE_RAW = {
    "location": "eastus",
    "tags": {"tag-new-key": "tag-new-value"},
    "properties": {
        "hardwareProfile": {"vmSize": "Standard_B1s"},
        "storageProfile": {
            "imageReference": {
                "sku": "18.04-LTS",
                "publisher": "Canonical",
                "version": "latest",
                "offer": "UbuntuServer",
            },
            "osDisk": {
                "caching": "ReadWrite",
                "managedDisk": {"id": None, "storageAccountType": "Standard_LRS"},
                "name": "myVMosdisk",
                "createOption": "FromImage",
                "deleteOption": "Detach",
                "diskSizeGB": 30,
            },
            "dataDisks": [
                {
                    "lun": 4,
                    "name": "data-disk-3",
                    "createOption": "Empty",
                    "caching": "ReadWrite",
                    "managedDisk": {
                        "id": "test-id",
                        "storageAccountType": "Premium_LRS",
                    },
                    "diskSizeGB": 8,
                    "deleteOption": "Detach",
                }
            ],
        },
        "osProfile": {"adminUsername": "admin123", "computerName": "myVM"},
        "networkProfile": {
            "networkInterfaces": [
                {
                    "id": "/subscriptions/subscription-id/resourceGroups/resource-group-1/providers/Microsoft.Network/networkInterfaces/nic-id-2",
                    "properties": {"primary": True},
                }
            ]
        },
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual machine. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.compute.virtual_machines.present = (
        hub.states.azure.compute.virtual_machines.present
    )
    mock_hub.exec.azure.compute.virtual_machines.get = (
        hub.exec.azure.compute.virtual_machines.get
    )
    mock_hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present
    )
    mock_hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine = (
        hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Compute/virtualMachines/{VM_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.compute.virtual_machines",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VM_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VM_NAME in url
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.compute.virtual_machines.present(
        test_ctx,
        name=RESOURCE_NAME,
        resource_group_name=RESOURCE_GROUP_NAME,
        virtual_machine_name=VM_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.compute.virtual_machines '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_both_returned_states(
        hub,
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=RESOURCE_NAME,
        resource_group_name=RESOURCE_GROUP_NAME,
        virtual_machine_name=VM_NAME,
        **RESOURCE_PARAMETERS,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.compute.virtual_machines '{RESOURCE_NAME}'" in ret["comment"]
    check_both_returned_states(
        hub,
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def _unmock_vm_conversion(hub, mock_hub):
    unmock_generic_conversion(hub, mock_hub)

    mock_hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present
    )

    mock_hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine = (
        hub.tool.azure.compute.virtual_machines.convert_present_to_raw_virtual_machine
    )

    mock_hub.tool.azure.compute.virtual_machines.convert_present_to_raw_image_reference = (
        hub.tool.azure.compute.virtual_machines.convert_present_to_raw_image_reference
    )


def _unmock_vm_update_payload_generation(hub, mock_hub):
    unmock_update_payload_generation(hub, mock_hub)

    mock_hub.tool.azure.compute.virtual_machines.update_virtual_machine_payload = (
        hub.tool.azure.compute.virtual_machines.update_virtual_machine_payload
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual machine. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.compute.virtual_machines.present = (
        hub.states.azure.compute.virtual_machines.present
    )
    mock_hub.exec.azure.compute.virtual_machines.get = (
        hub.exec.azure.compute.virtual_machines.get
    )

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    _unmock_vm_conversion(hub, mock_hub)
    _unmock_vm_update_payload_generation(hub, mock_hub)

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Compute/virtualMachines/{VM_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Compute/virtualMachines/{VM_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_UPDATE_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VM_NAME in url
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "hardwareProfile"
        ] == json.get("properties").get("hardwareProfile")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "networkProfile"
        ] == json.get("properties").get("networkProfile")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["properties"][
            "storageProfile"
        ] == json.get("properties").get("storageProfile")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["tags"] == json.get("tags")
        assert RESOURCE_PARAMETERS_UPDATE_RAW["location"] == json.get("location")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=RESOURCE_NAME,
        resource_group_name=RESOURCE_GROUP_NAME,
        virtual_machine_name=VM_NAME,
        **RESOURCE_PARAMETERS_UPDATE,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_both_returned_states(
        hub,
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=RESOURCE_PARAMETERS_UPDATE,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual machines. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.compute.virtual_machines.absent = (
        hub.states.azure.compute.virtual_machines.absent
    )
    mock_hub.exec.azure.compute.virtual_machines.get = (
        hub.exec.azure.compute.virtual_machines.get
    )
    mock_hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VM_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.compute.virtual_machines.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VM_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.compute.virtual_machines '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual machines. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.compute.virtual_machines.absent = (
        hub.states.azure.compute.virtual_machines.absent
    )
    mock_hub.exec.azure.compute.virtual_machines.get = (
        hub.exec.azure.compute.virtual_machines.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Compute/virtualMachines/{VM_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_UPDATE_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VM_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.compute.virtual_machines.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VM_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.compute.virtual_machines '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_both_returned_states(
        hub,
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS_UPDATE,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.compute.virtual_machines.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VM_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.compute.virtual_machines '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")

    check_both_returned_states(
        hub,
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS_UPDATE,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of virtual machines.
    """
    mock_hub.states.azure.compute.virtual_machines.describe = (
        hub.states.azure.compute.virtual_machines.describe
    )
    mock_hub.exec.azure.compute.virtual_machines.list = (
        hub.exec.azure.compute.virtual_machines.list
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present = (
        hub.tool.azure.compute.virtual_machines.convert_raw_virtual_machine_to_present
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Compute/virtualMachines/{VM_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_NAME,
                    **RESOURCE_PARAMETERS_UPDATE_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.compute.virtual_machines.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.compute.virtual_machines.present" in ret_value.keys()

    described_resource = ret_value.get("azure.compute.virtual_machines.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_both_returned_states(
        hub,
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS_UPDATE,
    )


def check_returned_states(
    hub,
    actual_state,
    expected_state,
):
    if actual_state:
        assert RESOURCE_GROUP_NAME == actual_state.get("resource_group_name")
        assert VM_NAME == actual_state.get("virtual_machine_name")

    hub.tool.azure.test_utils.check_actual_includes_expected(
        actual_state, expected_state, []
    )


def check_both_returned_states(
    hub,
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
):
    check_returned_states(
        hub,
        old_state,
        expected_old_state,
    )
    check_returned_states(
        hub,
        new_state,
        expected_new_state,
    )
