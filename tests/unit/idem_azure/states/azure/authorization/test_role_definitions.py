import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "my-role-definition"
ROLE_DEFINITION_ID = "b24988ac-6180-42a0-ab88-20f7382dd24c"
SCOPE = ""

RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "roleName": "Role name",
        "description": "Role description",
        "assignableScopes": ["/subscriptions/subId"],
        "permissions": [{"actions": ["action"], "notActions": []}],
    },
}
RESOURCE_PARAMETERS = {
    "role_definition_name": "Role name",
    "description": "Role description",
    "assignable_scopes": ["/subscriptions/subId"],
    "permissions": [{"actions": ["action"], "notActions": []}],
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role definitions. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.authorization.role_definitions.present = (
        hub.states.azure.authorization.role_definitions.present
    )
    mock_hub.exec.azure.authorization.role_definitions.get = (
        hub.exec.azure.authorization.role_definitions.get
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present = (
        hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_present_to_raw_role_definitions = (
        hub.tool.azure.authorization.role_definitions.convert_present_to_raw_role_definitions
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.authorization.role_definitions",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SCOPE in url
        assert ROLE_DEFINITION_ID in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SCOPE in url
        assert ROLE_DEFINITION_ID in url
        assert json == RESOURCE_PARAMETERS_RAW
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.authorization.role_definitions.present(
        test_ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_DEFINITION_ID,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )
    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    # Test present() with --test flag off
    ret = await mock_hub.states.azure.authorization.role_definitions.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_DEFINITION_ID,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role definitions. When a resource exists, 'present' will return the existing resource.
     Since Azure role definitions doesn't support PATCH operation
    """
    mock_hub.states.azure.authorization.role_definitions.present = (
        hub.states.azure.authorization.role_definitions.present
    )
    mock_hub.exec.azure.authorization.role_definitions.get = (
        hub.exec.azure.authorization.role_definitions.get
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present = (
        hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present
    )
    mock_hub.tool.azure.authorization.role_definitions.update_role_definitions_payload = (
        hub.tool.azure.authorization.role_definitions.update_role_definitions_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    resource_parameters_update = {
        "role_definition_name": "Role name 2",
        "description": "Role description 2",
        "assignable_scopes": ["/subscriptions/subId-2"],
        "permissions": [{"actions": ["action-2"], "notActions": ["notAction-2"]}],
    }
    resource_parameters_update_raw = {
        "properties": {
            "roleName": "Role name 2",
            "description": "Role description 2",
            "assignableScopes": ["/subscriptions/subId-2"],
            "permissions": [{"actions": ["action-2"], "notActions": ["notAction-2"]}],
        }
    }

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {
            "id": f"/{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SCOPE in url
        assert ROLE_DEFINITION_ID in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.authorization.role_definitions.present(
        test_ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_DEFINITION_ID,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )
    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.authorization.role_definitions.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_DEFINITION_ID,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Updated azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role definitions. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.authorization.role_definitions.absent = (
        hub.states.azure.authorization.role_definitions.absent
    )
    mock_hub.exec.azure.authorization.role_definitions.get = (
        hub.exec.azure.authorization.role_definitions.get
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present = (
        hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SCOPE in url
        assert ROLE_DEFINITION_ID in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.authorization.role_definitions.absent(
        ctx, RESOURCE_NAME, SCOPE, ROLE_DEFINITION_ID
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.authorization.role_definitions '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role definitions. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.authorization.role_definitions.absent = (
        hub.states.azure.authorization.role_definitions.absent
    )
    mock_hub.exec.azure.authorization.role_definitions.get = (
        hub.exec.azure.authorization.role_definitions.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present = (
        hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {
            "id": f"/{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SCOPE in url
        assert ROLE_DEFINITION_ID in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.authorization.role_definitions.absent(
        test_ctx, RESOURCE_NAME, SCOPE, ROLE_DEFINITION_ID
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.authorization.role_definitions.absent(
        ctx, RESOURCE_NAME, SCOPE, ROLE_DEFINITION_ID
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.authorization.role_definitions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of role definitions.
    """
    mock_hub.states.azure.authorization.role_definitions.describe = (
        hub.states.azure.authorization.role_definitions.describe
    )
    mock_hub.exec.azure.authorization.role_definitions.list = (
        hub.exec.azure.authorization.role_definitions.list
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present = (
        hub.tool.azure.authorization.role_definitions.convert_raw_role_definitions_to_present
    )
    mock_hub.tool.azure.authorization.role_definitions.get_resource_id = (
        hub.tool.azure.authorization.role_definitions.get_resource_id
    )
    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"
    resource_id = f"/{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}"
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.authorization.role_definitions.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.authorization.role_definitions.present" in ret_value.keys()
    described_resource = ret_value.get("azure.authorization.role_definitions.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert ROLE_DEFINITION_ID == old_state.get("role_definition_id")
        assert expected_old_state["role_definition_name"] == old_state.get(
            "role_definition_name"
        )
        assert expected_old_state["permissions"] == old_state.get("permissions")
        assert expected_old_state["description"] == old_state.get("description")
        assert expected_old_state["assignable_scopes"] == old_state.get(
            "assignable_scopes"
        )

    if new_state:
        assert ROLE_DEFINITION_ID == new_state.get("role_definition_id")
        assert expected_new_state["role_definition_name"] == new_state.get(
            "role_definition_name"
        )
        assert expected_new_state["permissions"] == new_state.get("permissions")
        assert expected_new_state["description"] == new_state.get("description")
        assert expected_new_state["assignable_scopes"] == new_state.get(
            "assignable_scopes"
        )
