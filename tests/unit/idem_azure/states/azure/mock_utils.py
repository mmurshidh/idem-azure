def unmock_generic_conversion(hub, mock_hub):
    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.tool.azure.utils.dict_add_nested_key_value_pair = (
        hub.tool.azure.utils.dict_add_nested_key_value_pair
    )


def unmock_update_payload_generation(hub, mock_hub):
    mock_hub.tool.azure.generic.compute_update_payload_for_key_subset = (
        hub.tool.azure.generic.compute_update_payload_for_key_subset
    )

    mock_hub.tool.azure.generic.get_update_payload = (
        hub.tool.azure.generic.get_update_payload
    )

    mock_hub.tool.azure.utils.cleanup_none_values = (
        hub.tool.azure.utils.cleanup_none_values
    )

    mock_hub.tool.azure.compare.compare_exact_matches = (
        hub.tool.azure.compare.compare_exact_matches
    )
