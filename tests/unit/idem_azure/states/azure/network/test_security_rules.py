import copy
from collections import ChainMap

import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NETWORK_SECURITY_GROUP_NAME = "my-security-group"
SECURITY_RULE_NAME = "my-security-rule"
RESOURCE_PARAMETERS = {
    "protocol": "*",
    "source_address_prefix": "10.0.0.0/8",
    "destination_address_prefix": "11.0.0.0/8",
    "access": "Deny",
    "destination_port_range": "8080",
    "source_port_range": "*",
    "priority": 100,
    "direction": "Outbound",
}
RAW_RESOURCE_PARAMETERS = {
    "properties": {
        "protocol": "*",
        "sourceAddressPrefix": "10.0.0.0/8",
        "destinationAddressPrefix": "11.0.0.0/8",
        "access": "Deny",
        "destinationPortRange": "8080",
        "sourcePortRange": "*",
        "priority": 100,
        "direction": "Outbound",
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of security rules. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.security_rules.present = (
        hub.states.azure.network.security_rules.present
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present = (
        hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present
    )
    mock_hub.tool.azure.network.security_rules.convert_present_to_raw_security_rules = (
        hub.tool.azure.network.security_rules.convert_present_to_raw_security_rules
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": f"Would create azure.network.security_rules '{RESOURCE_NAME}'",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        assert json == RAW_RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.security_rules.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Would create azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )
    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.security_rules.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert f"Created azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of security rules. When a resource exists, 'present' should update the resource.
    """
    mock_hub.states.azure.network.security_rules.present = (
        hub.states.azure.network.security_rules.present
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present = (
        hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present
    )
    mock_hub.tool.azure.network.security_rules.convert_present_to_raw_security_rules = (
        hub.tool.azure.network.security_rules.convert_present_to_raw_security_rules
    )
    mock_hub.tool.azure.network.security_rules.update_security_rules_payload = (
        hub.tool.azure.network.security_rules.update_security_rules_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    resource_parameters_update = {
        "protocol": "*",
        "source_address_prefix": "10.0.0.1/8",
        "destination_address_prefix": "12.0.0.0/8",
        "access": "Allow",
        "destination_port_range": "80",
        "source_port_range": "*",
        "priority": 105,
        "direction": "Outbound",
    }
    resource_parameters_update_raw = {
        "properties": {
            "protocol": "*",
            "sourceAddressPrefix": "10.0.0.1/8",
            "destinationAddressPrefix": "12.0.0.0/8",
            "access": "Allow",
            "destinationPortRange": "80",
            "sourcePortRange": "*",
            "priority": 105,
            "direction": "Outbound",
        },
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        assert resource_parameters_update_raw["properties"] == json["properties"]
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.security_rules.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Would update azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.security_rules.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert f"Updated azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of security rules. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.security_rules.absent = (
        hub.states.azure.network.security_rules.absent
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.security_rules.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.network.security_rules '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of security rules. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.security_rules.absent = (
        hub.states.azure.network.security_rules.absent
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.exec.azure.network.security_rules.get = (
        hub.exec.azure.network.security_rules.get
    )
    mock_hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present = (
        hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.security_rules.absent(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters
    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.security_rules.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.network.security_rules '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of network security groups.
    """
    mock_hub.states.azure.network.security_rules.describe = (
        hub.states.azure.network.security_rules.describe
    )
    mock_hub.exec.azure.network.security_rules.list = (
        hub.exec.azure.network.security_rules.list
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present = (
        hub.tool.azure.network.security_rules.convert_raw_security_rules_to_present
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
        f"{SECURITY_RULE_NAME}"
    )
    raw_nsg_resource = {
        "name": NETWORK_SECURITY_GROUP_NAME,
        "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
        "location": "southindia",
        "tags": {},
        "properties": {
            "securityRules": [
                {
                    "name": SECURITY_RULE_NAME,
                    "id": resource_id,
                    **RAW_RESOURCE_PARAMETERS,
                }
            ],
        },
    }
    expected_list = {
        "ret": {"value": [{**raw_nsg_resource}]},
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.security_rules.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.security_rules.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.security_rules.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert NETWORK_SECURITY_GROUP_NAME == old_state.get(
            "network_security_group_name"
        )
        assert SECURITY_RULE_NAME == old_state.get("security_rule_name")
        assert expected_old_state.get("protocol") == old_state.get("protocol")
        assert expected_old_state.get("source_address_prefix") == old_state.get(
            "source_address_prefix"
        )
        assert expected_old_state.get("destination_address_prefix") == old_state.get(
            "destination_address_prefix"
        )
        assert expected_old_state.get("access") == old_state.get("access")
        assert expected_old_state.get("destination_port_range") == old_state.get(
            "destination_port_range"
        )
        assert expected_old_state.get("source_port_range") == old_state.get(
            "source_port_range"
        )
        assert expected_old_state.get("priority") == old_state.get("priority")
        assert expected_old_state.get("direction") == old_state.get("direction")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert NETWORK_SECURITY_GROUP_NAME == new_state.get(
            "network_security_group_name"
        )
        assert SECURITY_RULE_NAME == new_state.get("security_rule_name")
        assert expected_new_state.get("protocol") == new_state.get("protocol")
        assert expected_new_state.get("source_address_prefix") == new_state.get(
            "source_address_prefix"
        )
        assert expected_new_state.get("destination_address_prefix") == new_state.get(
            "destination_address_prefix"
        )
        assert expected_new_state.get("access") == new_state.get("access")
        assert expected_new_state.get("destination_port_range") == new_state.get(
            "destination_port_range"
        )
        assert expected_new_state.get("source_port_range") == new_state.get(
            "source_port_range"
        )
        assert expected_new_state.get("priority") == new_state.get("priority")
        assert expected_new_state.get("direction") == new_state.get("direction")
