import copy
from collections import ChainMap

import pytest

from tests.unit.idem_azure.states.azure.mock_utils import unmock_generic_conversion
from tests.unit.idem_azure.states.azure.mock_utils import (
    unmock_update_payload_generation,
)

RESOURCE_NAME = "peer"
RESOURCE_IDEM_PATH = "azure.network.virtual_network_peerings"
PATH_PROPERTIES = {
    "subscription_id": "my-sub-id",
    "resource_group_name": "my-resource-group",
    "virtual_network_name": "my-vnet",
    "virtual_network_peering_name": "my-peerings",
}
RESOURCE_PARAMETERS = {
    "allow_virtual_network_access": True,
    "allow_forwarded_traffic": False,
    "allow_gateway_transit": True,
    "use_remote_gateways": False,
    "remote_virtual_network": {
        "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
    },
    "do_not_verify_remote_gateways": True,
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "allowVirtualNetworkAccess": True,
        "allowForwardedTraffic": False,
        "allowGatewayTransit": True,
        "useRemoteGateways": False,
        "remoteVirtualNetwork": {
            "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
        },
        "doNotVerifyRemoteGateways": True,
    }
}
RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/virtualNetworkPeerings/{virtual_network_peering_name}"


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual network peerings. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.virtual_network_peerings.present = (
        hub.states.azure.network.virtual_network_peerings.present
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )
    mock_hub.tool.azure.generic.run_present = hub.tool.azure.generic.run_present
    mock_hub.tool.azure.generic.run_put_request = hub.tool.azure.generic.run_put_request
    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.comment_utils.create_comment = (
        hub.tool.azure.comment_utils.create_comment
    )
    mock_hub.tool.azure.comment_utils.would_create_comment = (
        hub.tool.azure.comment_utils.would_create_comment
    )

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.virtual_network_peerings.get = (
        hub.exec.azure.network.virtual_network_peerings.get
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES),
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        for path_prop, path_prop_val in PATH_PROPERTIES.items():
            assert path_prop_val in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        for path_prop, path_prop_val in PATH_PROPERTIES.items():
            assert path_prop_val in url
        assert RESOURCE_PARAMETERS_RAW == json
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.virtual_network_peerings.present(
        test_ctx,
        RESOURCE_NAME,
        **PATH_PROPERTIES,
        **RESOURCE_PARAMETERS,
    )
    expected_resource_id = RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES)
    assert ret["result"], ret["comment"]
    assert f"Would create {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    assert ret["old_state"] is None
    assert ret["new_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.virtual_network_peerings.present(
        ctx,
        RESOURCE_NAME,
        **PATH_PROPERTIES,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert f"Created {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    assert ret["old_state"] is None
    assert ret["new_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of virtual network peerings. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.network.virtual_network_peerings.present = (
        hub.states.azure.network.virtual_network_peerings.present
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )

    mock_hub.tool.azure.generic.run_present = hub.tool.azure.generic.run_present
    mock_hub.tool.azure.generic.run_put_request = hub.tool.azure.generic.run_put_request

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.comment_utils.update_comment = (
        hub.tool.azure.comment_utils.update_comment
    )

    mock_hub.tool.azure.comment_utils.would_update_comment = (
        hub.tool.azure.comment_utils.would_update_comment
    )

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    mock_hub.exec.azure.network.virtual_network_peerings.get = (
        hub.exec.azure.network.virtual_network_peerings.get
    )

    unmock_generic_conversion(hub, mock_hub)
    unmock_update_payload_generation(hub, mock_hub)

    resource_parameters_update = {
        "allow_virtual_network_access": False,
        "allow_forwarded_traffic": True,
        "allow_gateway_transit": False,
        "use_remote_gateways": True,
        "remote_virtual_network": {
            "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
        },
        "do_not_verify_remote_gateways": False,
    }

    resource_parameters_update_raw = {
        "properties": {
            "allowVirtualNetworkAccess": False,
            "allowForwardedTraffic": True,
            "allowGatewayTransit": False,
            "useRemoteGateways": True,
            "remoteVirtualNetwork": {
                "id": "/subscriptions/subid/resourceGroups/peerTest/providers/Microsoft.Network/virtualNetworks/vnet2"
            },
            "doNotVerifyRemoteGateways": False,
        }
    }

    expected_get = {
        "ret": {
            "id": RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES),
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES),
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        for path_prop, path_prop_val in PATH_PROPERTIES.items():
            assert path_prop_val in url
        assert resource_parameters_update_raw == json
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.virtual_network_peerings.present(
        test_ctx,
        RESOURCE_NAME,
        **PATH_PROPERTIES,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert f"Would update {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    expected_resource_id = RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES)
    assert ret["old_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }
    assert ret["new_state"] == {
        **resource_parameters_update,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.virtual_network_peerings.present(
        ctx,
        RESOURCE_NAME,
        **PATH_PROPERTIES,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert f"Updated {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    assert ret["old_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }
    assert ret["new_state"] == {
        **resource_parameters_update,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual network peerings. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.virtual_network_peerings.absent = (
        hub.states.azure.network.virtual_network_peerings.absent
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )

    mock_hub.tool.azure.generic.run_absent = hub.tool.azure.generic.run_absent
    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.comment_utils.delete_comment = (
        hub.tool.azure.comment_utils.delete_comment
    )

    mock_hub.tool.azure.comment_utils.would_delete_comment = (
        hub.tool.azure.comment_utils.would_delete_comment
    )

    mock_hub.tool.azure.comment_utils.already_absent_comment = (
        hub.tool.azure.comment_utils.already_absent_comment
    )

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.virtual_network_peerings.get = (
        hub.exec.azure.network.virtual_network_peerings.get
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        for path_prop, path_prop_val in PATH_PROPERTIES.items():
            assert path_prop_val in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.virtual_network_peerings.absent(
        ctx, RESOURCE_NAME, **PATH_PROPERTIES
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert f"{RESOURCE_IDEM_PATH} '{RESOURCE_NAME}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of virtual network peerings. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.virtual_network_peerings.absent = (
        hub.states.azure.network.virtual_network_peerings.absent
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )

    mock_hub.tool.azure.generic.run_absent = hub.tool.azure.generic.run_absent
    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.comment_utils.delete_comment = (
        hub.tool.azure.comment_utils.delete_comment
    )

    mock_hub.tool.azure.comment_utils.would_delete_comment = (
        hub.tool.azure.comment_utils.would_delete_comment
    )

    mock_hub.tool.azure.comment_utils.already_absent_comment = (
        hub.tool.azure.comment_utils.already_absent_comment
    )

    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.virtual_network_peerings.get = (
        hub.exec.azure.network.virtual_network_peerings.get
    )

    expected_get = {
        "ret": {
            "id": RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES),
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        for path_prop, path_prop_val in PATH_PROPERTIES.items():
            assert path_prop_val in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.virtual_network_peerings.absent(
        test_ctx, RESOURCE_NAME, **PATH_PROPERTIES
    )
    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
    assert f"Would delete {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    expected_resource_id = RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES)
    assert ret["old_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.virtual_network_peerings.absent(
        ctx, RESOURCE_NAME, **PATH_PROPERTIES
    )
    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
    assert f"Deleted {RESOURCE_IDEM_PATH} '{RESOURCE_NAME}'" in ret["comment"]
    assert ret["old_state"] == {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "name": RESOURCE_NAME,
        "resource_id": expected_resource_id,
    }


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of virtual network peerings.
    """
    mock_hub.states.azure.network.virtual_network_peerings.describe = (
        hub.states.azure.network.virtual_network_peerings.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_raw_to_present_state
    )
    mock_hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state = (
        hub.tool.azure.network.virtual_network_peerings.convert_present_to_raw_state
    )

    mock_hub.tool.azure.generic.convert_state_format = (
        hub.tool.azure.generic.convert_state_format
    )

    mock_hub.exec.azure.network.virtual_network_peerings.list = (
        hub.exec.azure.network.virtual_network_peerings.list
    )

    resource_id = RESOURCE_ID_FORMAT.format(**PATH_PROPERTIES)
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.azure.network.virtual_networks.list.return_value = {
        "ret": [
            {
                "virtual_network_name": PATH_PROPERTIES["virtual_network_name"],
                "resource_group_name": PATH_PROPERTIES["resource_group_name"],
            }
        ],
        "result": True,
        "comment": "",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.virtual_network_peerings.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert f"{RESOURCE_IDEM_PATH}.present" in ret_value.keys()
    described_resource = ret_value.get(f"{RESOURCE_IDEM_PATH}.present")
    described_resource_map = dict(ChainMap(*described_resource))
    expected_state = {
        **RESOURCE_PARAMETERS,
        **PATH_PROPERTIES,
        "resource_id": resource_id,
    }
    expected_state["name"] = resource_id
    assert described_resource_map == expected_state
