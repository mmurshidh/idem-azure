import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest

RESOURCE_NAME = "my-resource"
name = "my-firewall"
RESOURCE_GROUP_NAME = "my-resource-group"
FIREWALL_NAME = "my-firewall"
location = "eastus"
tags = {"tag-key": "tag-value"}
SUBSCRIPTION_ID = "subscription_id"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "resource_group_name": RESOURCE_GROUP_NAME,
    "firewall_name": FIREWALL_NAME,
    "subscription_id": SUBSCRIPTION_ID,
    "tags": {"tag-key": "tag-value"},
}
RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "properties": {},
    "tags": {"tag-key": "tag-value"},
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of firewall. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.firewall.present = (
        hub.states.azure.network.firewall.present
    )
    mock_hub.tool.azure.network.firewall.convert_raw_firewall_to_present = (
        hub.tool.azure.network.firewall.convert_raw_firewall_to_present
    )
    mock_hub.tool.azure.network.firewall.convert_present_to_raw_firewall = (
        hub.tool.azure.network.firewall.convert_present_to_raw_firewall
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.firewall.get = hub.exec.azure.network.firewall.get

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/azureFirewalls/{FIREWALL_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert FIREWALL_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert FIREWALL_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.firewall.present(
        test_ctx, RESOURCE_NAME, **RESOURCE_PARAMETERS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Would create azure.network.firewall '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.firewall.present(
        ctx, RESOURCE_NAME, **RESOURCE_PARAMETERS
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.network.firewall '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of firewall. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.network.firewall.present = (
        hub.states.azure.network.firewall.present
    )
    mock_hub.tool.azure.network.firewall.convert_raw_firewall_to_present = (
        hub.tool.azure.network.firewall.convert_raw_firewall_to_present
    )
    mock_hub.tool.azure.network.firewall.convert_present_to_raw_firewall = (
        hub.tool.azure.network.firewall.convert_present_to_raw_firewall
    )
    mock_hub.tool.azure.network.firewall.update_firewall_payload = (
        hub.tool.azure.network.firewall.update_firewall_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.firewall.get = hub.exec.azure.network.firewall.get

    resource_parameters_update = {
        "location": "eastus",
        "resource_group_name": RESOURCE_GROUP_NAME,
        "firewall_name": FIREWALL_NAME,
        "subscription_id": SUBSCRIPTION_ID,
        "tags": {"tag-key": "tag-value"},
    }

    resource_parameters_update_raw = {
        "location": "eastus",
        "properties": {},
        "tags": {"tag-new-key": "tag-new-key"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/azureFirewalls/{FIREWALL_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/azureFirewalls/{FIREWALL_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert FIREWALL_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.firewall.present(
        test_ctx,
        RESOURCE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"azure.network.firewall '{RESOURCE_NAME}' doesn't need to be updated."
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.firewall.present(
        ctx,
        RESOURCE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of firewall. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.firewall.absent = (
        hub.states.azure.network.firewall.absent
    )
    mock_hub.tool.azure.network.firewall.convert_raw_firewall_to_present = (
        hub.tool.azure.network.firewall.convert_raw_firewall_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.firewall.get = hub.exec.azure.network.firewall.get

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert FIREWALL_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.firewall.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, FIREWALL_NAME, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert f"azure.network.firewall '{FIREWALL_NAME}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of firewall. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.firewall.absent = (
        hub.states.azure.network.firewall.absent
    )
    mock_hub.tool.azure.network.firewall.convert_raw_firewall_to_present = (
        hub.tool.azure.network.firewall.convert_raw_firewall_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.firewall.get = hub.exec.azure.network.firewall.get

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/azureFirewalls/{FIREWALL_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert FIREWALL_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.firewall.absent(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        FIREWALL_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Would delete azure.network.firewall '{FIREWALL_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.firewall.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, FIREWALL_NAME, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.network.firewall '{FIREWALL_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of firewall.
    """
    mock_hub.states.azure.network.firewall.describe = (
        hub.states.azure.network.firewall.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.firewall.convert_raw_firewall_to_present = (
        hub.tool.azure.network.firewall.convert_raw_firewall_to_present
    )
    mock_hub.exec.azure.network.firewall.list = hub.exec.azure.network.firewall.list

    resource_id = (
        f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/azureFirewalls/{FIREWALL_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_GROUP_NAME,
                    **RESOURCE_PARAMETERS_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.firewall.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.firewall.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.firewall.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert FIREWALL_NAME == old_state.get("firewall_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert FIREWALL_NAME == new_state.get("firewall_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")
