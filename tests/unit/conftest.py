import sys
import unittest.mock as mock

import pop.hub
import pytest
from dict_tools import data


@pytest.fixture(scope="function", name="hub")
def unit_hub(code_dir):
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        for dyne in ["idem", "pop_create"]:
            hub.pop.sub.add(dyne_name=dyne)
        hub.idem.RUNS = {"test": {}}
        yield hub


@pytest.fixture(scope="module")
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    fake_subscription = "12345678-1234-1234-1234-aaabc1234aaa"
    fake_client_id = "76543210-4321-4321-4321-bbbb3333aaaa"
    fake_secret = "ZzxxxXXXX11xx-aaaaabbbb-k3xxxxxx"
    tenant = "bbbbbca-3333-4444-aaaa-cddddddd6666"
    yield data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        acct=data.NamespaceDict(
            endpoint_url=f"https://management.azure.com",
            client_id=fake_client_id,
            secret=fake_secret,
            subscription_id=fake_subscription,
            tenant=tenant,
            headers=dict(),
        ),
    )
