import copy
import os
import uuid
from random import random
from typing import Any
from typing import Dict
from typing import List

import yaml
from pytest_idem import runner


def call_present_from_properties(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
    additional_kwargs: List[str] = None,
) -> Dict:
    name = present_state_properties["name"]
    # yaml representer does not handle NamespaceDict class, clone it in order to have it as dict
    present_state_properties = copy.deepcopy(copy.copy(present_state_properties))
    present_state_str = yaml.safe_dump(
        {
            name: {
                f"azure.{resource_type}.present": [
                    {k: v} for k, v in present_state_properties.items()
                ]
            }
        }
    )

    present_state_ret = hub.tool.azure.test_utils.run_idem_state(
        idem_cli, present_state_str, test, additional_kwargs
    )
    return hub.tool.azure.test_utils.get_esm_tagged_data(
        present_state_ret, f"azure.{resource_type}"
    )


async def call_absent(
    hub,
    ctx,
    idem_cli,
    resource_type: str,
    name: str,
    resource_id: str,
    test: bool = False,
    absent_params: Dict[str, str] = None,
    additional_kwargs: List[str] = None,
    wait_for_absent: bool = False,
) -> Dict:
    params_string = ""
    if absent_params:
        for absent_param, param_value in absent_params.items():
            params_string += " " * 14
            param_string = f"- {absent_param}: {param_value}"
            params_string += f"{param_string}\n"

    if resource_id:
        absent_state_str = f"""
            {name}:
              azure.{resource_type}.absent:
              - resource_id: {resource_id}"""
    else:
        absent_state_str = f"""
            {name}:
              azure.{resource_type}.absent:
              - name: {name}"""

    if params_string:
        absent_state_str = f"{absent_state_str}\n{params_string}"

    absent_state_ret = hub.tool.azure.test_utils.run_idem_state(
        idem_cli, absent_state_str, test, additional_kwargs
    )
    absent_state_ret = hub.tool.azure.test_utils.get_esm_tagged_data(
        absent_state_ret, f"azure.{resource_type}"
    )
    if wait_for_absent and absent_state_ret.get("old_state"):
        api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
        # resource was deleted as a result of this call
        await hub.tool.azure.resource.wait_for_absent(
            ctx,
            url=f"{ctx.acct.endpoint_url}{resource_id}?api-version={api_version}",
            retry_count=10,
            retry_period=25,
        )
    return absent_state_ret


def run_idem_state(
    hub,
    idem_cli,
    state: str,
    test: bool = False,
    additional_args: List = None,
    acct_profile: str = "test_development_idem_azure",
):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(state)
        args = [f"--acct-profile={acct_profile}"]
        if additional_args:
            for item in additional_args:
                args.append(item)
        if test:
            args.append("--test")
        return idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json


def get_esm_tagged_data(hub, data: Dict, resource_type: str = None):
    ret = None
    found: bool = False
    for key, value in data.items():
        if "_|-" in key:
            if resource_type:
                res_type, _, _, _ = key.split("_|-", maxsplit=3)
                if res_type != resource_type:
                    continue
            if found:
                raise ValueError("Duplicate matching tags found!")
            ret = value
            found = True

    if not found:
        raise ValueError("No matching tag found!")

    return ret


def generate_unique_name(hub, name_prefix: str, max_length: int = 62) -> str:
    r"""Uses the CI_MERGE_REQUEST_IID and JOB_ID environment variable to generate unique name.
    This can help associate the resource with the pipeline that has created it. If the test is
    run locally the second part is missing:
        {name_prefix}{MERGE_REQUEST_IID-JOB_ID}?-{random-part}

    Args:
        name_prefix: name prefix between 2 and 42 characters.
        max_length: the maximum number of characters of the returned name. Default is 62.

    Returns:
        random name up to 62 characters in length. The returned name guaranteed will not end
        with '-' but with number or letter.

    Examples:
        generate_unique_name(None) -> ValueError("name is None")
        generate_unique_name('') -> ValueError("name is too short")
        generate_unique_name('this-is-a-very-long-name-prefix-0-1-2-3-4-5') -> ValueError("name is too long")

        standalone:  generate_unique_name('idem-test') -> idem-azure-3m2422oxdcygx87wvmo3pffkw
        standalone:  generate_unique_name('idem-test', 20) -> idem-azure-3m2422oxdcy
        in pipeline: generate_unique_name('idem-test') -> idem-test-123-3997117126-3m2422oxdcygx87wvmo3pffkw
    """

    if not name_prefix:
        raise ValueError("name is None")

    if len(name_prefix) < 2:
        raise ValueError("name is too short: " + name_prefix)

    if len(name_prefix) > 42:
        raise ValueError("name is too long: " + name_prefix)

    if max_length < 10:
        raise ValueError(
            "max_length bigger than 9 is required, current is: " + max_length
        )

    result = name_prefix + "-"

    merge_request_id = os.environ.get("CI_MERGE_REQUEST_IID")
    if merge_request_id:
        result += merge_request_id.lower() + "-"

    job_id = os.environ.get("CI_JOB_ID")
    if job_id:
        result += job_id.lower() + "-"

    chars = "0123456789abcdefghijklmnopqrstuvwxyz"
    result += __to_base(uuid.uuid4().int, 36, chars)
    result = result[:max_length]

    if result[-1] == "-":
        result = result[:-1] + random.choice(chars)

    return result


def check_actual_includes_expected(
    hub, actual: Any, expected: Any, ignore_props: List[str]
):
    if isinstance(expected, dict):
        assert isinstance(actual, dict), f"Expected dictionary, got {actual}"
        for prop, expected_value in expected.items():
            if prop in ignore_props:
                continue
            if expected_value is None:
                continue
            assert prop in actual, f"{prop} not in {actual}"
            ignored_prefix = f"{prop}."
            new_ignore_props = [
                ignored_prop[len(ignored_prefix) :]
                for ignored_prop in ignore_props
                if ignored_prop.startswith(ignored_prefix)
            ]
            hub.tool.azure.test_utils.check_actual_includes_expected(
                actual.get(prop), expected_value, new_ignore_props
            )
    elif isinstance(expected, list):
        assert isinstance(actual, list), f"Expected list, got {actual}"
        assert len(actual) == len(
            expected
        ), f"Expected lists to have equal lengths: {actual}, {expected}"
        new_ignore_props = [
            ignored_prop[3:]
            for ignored_prop in ignore_props
            if ignored_prop.startswith("[].")
        ]
        for index, list_item in enumerate(expected):
            hub.tool.azure.test_utils.check_actual_includes_expected(
                actual[index], list_item, new_ignore_props
            )
    else:
        assert actual == expected, f"Expected {expected}, got {actual}"


def __to_base(s, b, chars):
    res = ""
    while s:
        res += chars[s % b]
        s //= b
    return res[::-1] or "0"
